from random import randint
from time import sleep
import pylab

class Vide:
    '''Classe qui permet de voir si la mer est vide.'''
    
    def __init__(self, identite = None):
        ''' Constructeur de la classe Vide.
: param: l'objet de la classe,
         l'identite.
: pramtype: self(obj), identite(bool)
: return: None
'''
        self.__identite = identite
    
    def get_identite(self):
        ''' Accesseur de l'attribut identite de la classe Vide.
: pram: l'objet de la classe
: pramtype: self(obj)
: return: l'attribut privé identite
: returntype: type(identite)
'''
        return self.__iendentite
    
    def __str__(self):
        '''Méthode d'affichage de la classe Vide.
: param: l'objet de la classe
: paramtype: self(obj)
: return: un espace vide
: returntype: espace(str)
'''
        return ' '

class Requin:
    '''Classe thon qui permet de déterminer si un requin peut de reproduire, connaître ses voisins
ou connaître son temps de gestation.'''
    
    def __init__(self, R, energie_requin, duree_gestation_requin, voisins):
        '''Constructeur de la classe Requin.
: param: l'objet de la classe Requin,
         les requins,
         l'energie des requins,
         la duree de gestation des requins,
         les voisins
: paramtype:self(obj), requins(str), energie(int), duree(int), voisins()
: return: None
'''
        self.__R = R
        self.__energie_requin = energie_requin
        self.__duree_gestation_requin = duree_gestation_requin
        self.__voisins = voisins
    
    def __str__(self):
        '''Methode d'affichage.
: param: l'objet de la classe Requin.
: paramtype: self(obj)
: return: les requins
: returntype: requins()
'''
        return str(self.get_R())
    
    def mort(self):
        '''Méthode qui determine si un requin doit mourir ou non.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: prédicate oui: le requin meurt non:le requin a encore assez d'énergie pour vivre
: returntype: prédicat(bool)
'''
        if self.get_energie_requin() == 0:
            return True
        else:
            return False
    
    def reproduction(self):
        '''Methode qui détermine si un requin peut se reproduire ou non.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: prédicate oui: le requin peut se reproduire non:le requin ne peut pas se reproduire
: returntype: prédicat(bool)
        if self.get_duree_gestation_requins<=0:
            return True
        else:
            return False
            '''
        if self.get_duree_gestation_requin() == 0:
            return True
        else:
            return False
    
    def get_R(self):
        '''Accesseur de l'attribut R de la classe Requin.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: l'attribut privé 'R' de la classe Requin
: returntype:
'''
        return self.__R
    
    def get_energie_requin(self):
        '''Accesseur de l'attribut energie_requin de la classe Requin.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: l'attribut privé 'energie_requin' de la classe Requin
: returntype:
'''
        return self.__energie_requin
    
    def get_duree_gestation_requin(self):
        '''Accesseur de l'attribut R de la classe Requin.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: l'attribut privé 'R' de la classe Requin
: returntype:
'''
        return self.__duree_gestation_requin
    
    def get_voisins(self):
        '''Accesseur de l'attribut voisins de la classe Requin.
: param: l'objet de la classe Requin
: paramtype: self(obj)
: return: l'attribut privé 'voisins' de la classe Requin
: returntype:
'''
        return self.__voisins
    
    def set_energie_requin(self, param):
        '''Mutateur de l'attribut energie_requin de la classe Requin.
: param: l'objet de la classe Requin
         le nouveau paramètre de l'attribut energie_requin
: paramtype: self(obj), param(int)
: return: l'attribut privé 'energie_requin' de la classe Requin
: returntype:
'''
        self.__energie_requin = param
    
    def set_duree_gestation_requin(self, param):
        '''Mutateur de l'attribut energie_requin de la classe Requin.
: param: l'objet de la classe Requin
         le nouveau paramètre de l'attribut duree_gestation_requin
: paramtype: self(obj), param(int)
: return: l'attribut privé 'energie_requin' de la classe Requin
: returntype:
'''
        self.__duree_gestation_requin = param
    
    def set_voisins(self, param):
         '''Mutateur de l'attribut voisins de la classe Requin.
: param: l'objet de la classe Requin
         le nouveau paramètre de l'attribut voisins
: paramtype: self(obj), param(int)
: return: l'attribut privé 'voisins' de la classe Requin
: returntype:
'''
         self.__voisins = param

class Thon:
    '''Classe thon qui permet de déterminer si un thon peut se reproduire, connaître ses voisins
ou connaître son temps de gestation.'''
    
    def __init__(self, T, temps_de_gestation, voisins = None):
        '''Constructeur de la classe Thon.
: pram: l'objet de la classe Thon,
        les thons,
        le temps de gestation,
        les voisins.
: paramtype: self(obj), T(str), temps_de_gestation(int), voisins(bool)
: return: None
'''
        self.__T = T
        self.__temps_de_gestation = temps_de_gestation
        self.__voisins = voisins
       
    def __str__(self):
        '''Methode d'affichage de la classe Thon.
: param: l'objet de la classe Thon
: paramtype: self(obj)
: return: les thons symbolisés par 'T'
: returntype: thons(str)
'''
        return str(self.get_T())
    
    def reproduction(self):
        '''Méthode qui testera la valeur du temps de gestation et décidera si il y a naissance ou non.
: param: l'objet de la classe Thon
: paramtype: self(obj)
: return: True: il peut y avoir naissance de thons, False: il ne peut y avoir naissance de thons
: returntype: reponse(bool)
'''
        if self.get_temps_de_gestation() ==0:
            return True
        else:
            return False
    
    def get_T(self):
        '''Accesseur de l'attribut 'T' de la classe Thon.
: param: l'objet de la classe Thon
: paramtype: self(obj)
: return: l'attribut privé 'T' de la classe Thon
: returntype: T(str)
'''
        return self.__T
    
    def get_temps_de_gestation(self):
        '''Accesseur de l'attribut 'temps_de_gestation' de la classe Thon.
: param: l'objet de la classe Thon
: paramtype: self(obj)
: return: l'attribut privé 'temps_de_gestation' de la classe Thon
: returntype: temps_de_gestation(int)
'''
        return self.__temps_de_gestation
    
    def get_voisins(self):
        '''Accesseur de l'attribut 'voisins' de la classe Thon.
: param: l'objet de la classe Thon
: paramtype: self(obj)
: return: l'attribut privé 'voisins' de la classe Thon
: returntype: voisins(int)
'''
        return self.__voisins
    
    def set_temps_de_gestation(self, param):
        '''Mutateur de l'attribut 'temps_de_gestation' de la classe Thon.
: param: l'objet de la classe Thon,
         nouvel valeur de l'attribut 'temps_de_gestation'
: paramtype: self(obj), temps_de_gestation(int)
: return: None
'''
        self.__temps_de_gestation = param
    
    def set_voisins(self, param):
         '''Mutateur de l'attribut 'voisins' de la classe Thon.
: param: l'objet de la classe Thon,
         nouvelle valeur de l'attribut 'voisins'
: paramtype: self(obj), temps_de_gestation(int)
: return: None
'''
         self.__voisins = param
    
def grille_vide(hauteur, largeur):
    '''Fonction qui crée une grille de hauteur et largeur choisies par l'utlisateur dont les éléments sont
tous des objets de la classe vide
: param: la hauteur de la grille,
         la largeur de la grille.
: paramtype: hauteur(int), largeur(int)
: return: une grille vide
: returntype: grille(list)
'''
    contenu = Vide()
    return [[str(contenu) for i in range(largeur)]for j in range(hauteur)]

def tirage_case(hauteur, largeur):
    '''Fonction qui réalise un tirage au sort de la hauteur et de la largeur.
: param: la hauteur de la grille de jeu,
         la largeur de la grille de jeu.
: paramtype: hauteur(int), largeur(int)
: return: un tuple contenant le tirage aléatoire de la hauteur et de la largeur de la grille.
: returntype: resultat(tuple)
'''
    hauteur = randint(0, hauteur+1)
    largeur = randint(0, largeur+1)
    coordonnee = (hauteur,largeur)
    return coordonnee

def position_initiale_vivants(hauteur, largeure, nombre_de_thons, nombre_de_requins):
    '''Fonction qui renvoie les coordonnées d'un être vivant sur la mer ainsi que le nombre total d'êtres vivants.
: param: la hauteur qui correspond à la hauteur de la grille,
         la largeur qui correspond à la largeur de la grille,
         le nombre de thons initialement présent dans la grille,
         le nombre de requins initialement présents dans la grille.
: paramtype: hauteur(int), largeur(int), nombre_de_thons(int), nombre_de_requins(int)
: return: renvoie une liste de tuples contenant les coordonnées de chaque être vivant dans la mer,
          le nombre total d'êtres vivants présent dans la mer.
: returntype: ensemble des coordonnées(list), coordonnées(tuple), nombre d'être vivant(int).
'''
    mer = []
    taille_mer = hauteur * largeure
    nombre_individus = nombre_de_thons + nombre_de_requins
    if nombre_individus > taille_mer:
        return 'Impossible'
    else:
        while nombre_de_thons != 0:
            case = tirage_case((hauteur-2),(largeure-2))
            for _ in range(len(mer)):
                for coor in mer:
                    if case == coor:
                        while case == coor:
                            case = tirage_case((hauteur-2),(largeure-2))
            mer.append(case)
            nombre_de_thons = nombre_de_thons - 1
        while nombre_de_requins != 0:
            case = tirage_case((hauteur-2),(largeure-2))
            for _ in range(len(mer)):
                for coor in mer:
                    if case == coor:
                        while case == coor:
                            case = tirage_case((hauteur-2),(largeure-2))
            mer.append(case)
            nombre_de_requins = nombre_de_requins - 1
        return mer

def création_grille_jeu(hauteur, largeure, nombre_requins, nombre_thons, duree_gestation_requins, energie_requins, duree_gestation_thons):
    '''Fonction qui renvoie une grille de jeu remplie de thons, de requins ou vide.
: param: la hauteur de la grille de jeu,
         la largeur de la grille de jeu,
         le nombre de requins initial dans la grille,
         le nombre de thons initial dans la grille,
         la durée de gestation des requins,
         l'énergie des requins,
         la duree de gestation des thons.
: paramtype: hauteur(int), largeur(int), nombre_requins(int), nombre_thons(int), duree_gestation_requins(int), energie_requins(int), duree_gestation_thons(int)
: return: une grille de jeu remplie de thons, requins ou vide
: returntype: grille(list)
'''
    requin = Requin('R', energie_requins, duree_gestation_requins, [])
    thon = Thon('T', duree_gestation_thons, [])
    vide = Vide()
    liste = position_initiale_vivants(hauteur, largeure, nombre_thons, nombre_requins)
    longueure_plateau = len(liste)
    plateau = grille_vide(hauteur,largeure)
    n = 0
    for _ in range(nombre_thons):
        coordonnee = liste[n]
        x = coordonnee[0]
        y = coordonnee[1]
        plateau[x][y] = str(thon)
        n = n + 1
    for _ in range(nombre_requins):
        coordonnee = liste[n]
        x = coordonnee[0]
        y = coordonnee[1]
        plateau[x][y] = str(requin)
        n = n + 1
    return plateau
     
def regles_vivants_thons( grille_jeu, abscisse, ordonnee, duree_gestation):
    '''Fonction qui gère l'ensemble des règles de vie d'un thon.
: param: la fonction création_grille_jeu(),
         son ordonnée,
         son abscisse,
         sa durée de gestation.
: paramtype: création_grille_jeu(func), ordonnee(int), abscisse(int), duree_gestation(int)
: return: None
'''
    nb_thons = 0
    nb_requins = 0
    a = 0
    nb_elements_1 = len(grille_jeu)
    while a < nb_elements_1:
        nb_elements_2 = len(grille_jeu[a])    
        b = 0
        while b < nb_elements_2:
            identite = grille_jeu[a][b]
            if identite == 'T':
                nb_thons = nb_thons + 1
            if a == abscisse and b == ordonnee:
                thons = nb_thons
                requins = nb_requins
            else:
                if a == abscisse and b == ordonnee:
                    print('il n\'y a aucun être vivant dans la grille')
            b = b + 1
        a = a + 1
    plateau = recherche_voisins(grille_jeu)
    voisins = plateau[0][thons-1]
    if duree_gestation == 0:
        reproduction = 'True'
    else:
        reproduction = 'False'
    return reproduction , recherche_case_libre([[(voisins)]])
    

def regles_vivants_requins(grille_jeu,abscisse, ordonnee, duree_gestation, energie):
    '''Fonction qui gère l'ensemble des règles de vie d'un requin.
: param: la fonction création_grille_jeu(),
         son ordonnée,
         son abscisse,
         sa durée de gestation,
         son énergie.
: paramtype: création_grille_jeu(func), ordonnee(int), abscisse(int), duree_gestation(int), energie(int)
: return: None
'''
    nb_thons = 0
    nb_requins = 0
    a = 0
    nb_elements_1 = len(grille_jeu)
    while a < nb_elements_1:
        nb_elements_2 = len(grille_jeu[a])    
        b = 0
        while b < nb_elements_2:
            identite = grille_jeu[a][b]
            if identite == 'T':
                nb_thons = nb_thons + 1
            elif identite == 'R':
                nb_requins = nb_requins + 1
            if a == abscisse and b == ordonnee:
                thons = nb_thons
                requins = nb_requins
            else:
                if a == abscisse and b == ordonnee:
                    print('il n\'y a aucun être vivant dans la grille')
            b = b + 1
        a = a + 1
    
    if duree_gestation == 0:
        reproduction = 'True'
    else:
        reproduction = 'False'
    if energie == 0:
        mort = 'True'
    else:
        mort = 'False'
    
    
    plateau = recherche_voisins(grille_jeu)
    voisins = plateau[1][requins-1]
    h = recherche_case_libre([[(voisins)]])
    i = recherche_case_thon([[(voisins)]])
    return reproduction, mort, h + i
    
    

def recherche_voisins(grille_jeu):
    '''Fonction qui crée une grille de jeu et renvoie les voisins 'G', 'D', 'H', 'B'.
: param: fonction 'création_grille_jeu' qui permet de créer un grille de jeu préremplie.
: paramtype: grille(liste)
: return: l'ensemble des voisins dans une liste
: returntype: ensemble_voisins(list), voisins(tuples)
'''
    voisins_requins = []
    voisins_thons = []
    nb_elements_1 = len(grille_jeu)
    a = 0
    a_min = 0
    a_max = len(grille_jeu)-1
    b_min = 0
    b_max = len(grille_jeu[0])-1
    while a < nb_elements_1:
        nb_elements_2 = len(grille_jeu[a])    
        b = 0
        while b < nb_elements_2:
            a_bis = a-1
            b_bis = b-1
            if grille_jeu[a][b] == 'R':
                if a == a_min and b == b_min: ##1
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_min and b == b_max: ##2
                    B = grille_jeu[a][0]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b == b_min: ##3
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[0][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b == b_max: ##4
                    B = grille_jeu[a][0]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[0][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b != b_min and b != b_max: ##8
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[0][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_min and a != a_max and b != b_min: ##7
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a != a_max and a != a_min and b == b_max: ##5
                    B = grille_jeu[a][0]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a != a_max and a != a_min and b == b_min: ##6
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]
                    
                else:                                          ##9
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_requins.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    requins = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]
                
            elif grille_jeu[a][b] == 'T':
                if a == a_min and b == b_min: ##1
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_min and b == b_max: ##2
                    B = grille_jeu[a][0]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b == b_min: ##3
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[0][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b == b_max: ##4
                    B = grille_jeu[a][0]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[0][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_max and b != b_min and b != b_max: ##8
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[0][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a == a_min and a != a_max and b != b_min: ##7
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a-1][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a != a_max and a != a_min and b == b_max: ##5
                    B = grille_jeu[a][0]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b_bis]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]

                elif a != a_max and a != a_min and b == b_min: ##6
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]
                else:                                          ##9
                    B = grille_jeu[a][b+1]
                    G = grille_jeu[a_bis][b]
                    H = grille_jeu[a][b-1]
                    D = grille_jeu[a+1][b]
                    voisins_thons.append((('B:',B),(' G:',G),(' H:',H),(' D:',D)))
                    thons = [('B:',B),(' G:',G),(' H:',H),(' D:',D)]
                
            b = b + 1
        a = a + 1
    return(voisins_thons,voisins_requins)
    
    
def contenu_cases(grille_jeu, ordonnee, abscisse, duree_gestation_requins, duree_gestation_thons, energie):
    '''Fonction qui identifie ce qui se trouve dans la case de jeu.
: param: la fonction 'création_grille_jeu',
         l'ordonnee de la case à identifier,
         l'abscisse de la case à identifier,
         la durée de gestation des requins,
         la durée de gestation des thons,
         l'énergie des requins.
: paramtype: création_grille_jeu(func), ordonnee(int), abscisse(int), duree_gestation_requins(int), duree_gestation_thon(int), energie(int)
: return: le contenu d'une case de la grille de jeu
: returntype: contenu(tuple)
'''
    identite = grille_jeu[abscisse][ordonnee]
    if identite == 'R':
        regles_vivants_requins(ordonnee, abscisse, duree_gestation_requins, energie)
    elif identite == 'T':
        regles_vivants_thons( grille_jeu, ordonnee, abscisse, duree_gestation_thons)
    else:
        return 'La case est vide'

def recherche_case_libre(liste_voisins):
    '''Fonction qui renvoie une liste contenant les cases dans les objets ont étés instanciés par la classe Vide.
: param: fonction recherche_voisins
: paramtype: recherche_voisins(func)
: return: une listes de cases vides de la grille
: returntype: case_libre(list)
'''
    case_libres = []
    liste_voisins = liste_voisins[0]
    for parties in liste_voisins:
        for case in parties:
            if case[1]==' ':
                case_libres.append(case)
    
    if len(case_libres)==0:
        return 'Il n\'y a aucune case libre'
    return case_libres

def deplacement(grille_jeu, abscisse, ordonnee, case_voisine):
    '''Fonction qui deplace l'être vivant dans la case indiquée en prenant en compte ses règles et la mer 'fermée'.
: param: la fonction création_grille_jeu(),
         l'ordonnee de la case choisie,
         l'abscisse de la case choisie,
         la case dans laquelle effectuer le déplacement.
: paramtype: création_grille_jeu(func), ordonnee(int), abscisse(int), case_voisine(str)
: return: None 
'''
    
    identite = grille_jeu[abscisse][ordonnee]
    affichage(grille_jeu)
    nb_elements_1 = len(grille_jeu)
    a = 0
    a_min = 0
    a_max = len(grille_jeu)-1
    b_min = 0
    b_max = len(grille_jeu[0])-1
    while a < nb_elements_1:
        nb_elements_2 = len(grille_jeu[a])    
        b = 0
        while b < nb_elements_2:
            if (a == abscisse and b == ordonnee and a == a_min and b == b_min):##1
                coin_gauche_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
                
            elif (a == abscisse and b == ordonnee and a == a_min and b == b_max): ##2
                coin_gauche_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
            
            elif (a == abscisse and b == ordonnee and a == a_max and b == b_min): ##3
                coin_droit_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
            
            elif (a == abscisse and b == ordonnee and a == a_max and b == b_max): ##4
                coin_droit_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee)      
                return(affichage(grille_jeu))
            
            elif (a == abscisse and b == ordonnee and a == a_max and b != b_min and b != b_max): ##8
                coté_droit(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
            
            elif (a == abscisse and b == ordonnee and a == a_min and a != a_max and b != b_min): ##7
                coté_gauche(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
            
            elif (a == abscisse and b == ordonnee and a != a_max and a != a_min and b == b_max): ##5
                coté_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
                    
            elif (a == abscisse and b == ordonnee and a != a_max and a != a_min and b == b_min): ##6
                coté_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
                    
            elif(a == abscisse and b == ordonnee and a!=a_max and a != a_min and b!=b_max and b!=b_min):                                          ##9
                millieu(a,b,grille_jeu,case_voisine,abscisse,ordonnee)
                return(affichage(grille_jeu))
                    
            
            
            b = b + 1
        a = a + 1

def coin_gauche_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a-1][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b-1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def coin_gauche_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    b_bis = b-1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][0] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a-1][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b_bis] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def coin_droit_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b-1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[0][0] = identite
        grille_jeu[a][b] = str(Vide())

def coin_droit_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    b_bis = b - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][0] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b_bis] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[0][b] = identite
        grille_jeu[a][b] = str(Vide())

def coté_droit(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    b_bis = b - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b_bis] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[0][b] = identite
        grille_jeu[a][b] = str(Vide())

def coté_gauche(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    b_bis = b - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a-1][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b_bis] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def coté_bas(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    b_bis = b - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][0] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b_bis] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def coté_haut(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b-1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def millieu(a,b,grille_jeu,case_voisine,abscisse,ordonnee):
    a_bis = a - 1
    identite = grille_jeu[abscisse][ordonnee]
    if case_voisine == 'B':
        grille_jeu[a][b+1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'G':
        grille_jeu[a_bis][b] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'H':
        grille_jeu[a][b-1] = identite
        grille_jeu[a][b] = str(Vide())
    elif case_voisine == 'D':
        grille_jeu[a+1][b] = identite
        grille_jeu[a][b] = str(Vide())

def recherche_case_thon(liste_voisins):
    '''Fonction qui renvoie une liste contenant les cases dont les objets ont été instanciés par la classe Thon.
: param: fonction recherche_voisins
: paramtype: recherche_voisins(func)
: return: une listes de cases Thon de la grille
: returntype: case_thon(list)
'''
    case_thons = []
    liste_voisins = liste_voisins[0]
    for parties in liste_voisins:
        for case in parties:
            if case[1]=='T':
                case_thons.append(case)
                
    if len(case_thons)==0:
        return 'Il n\'y a pas de thon dans les cases voisnes'
    return case_thons

def affichage(grille):
    '''Fonction d'affichage de la grille de jeu en 2D.
: param: la fonction 'creation_grille_jeu'
: paramtype: creation_grille_jeu(func)
: return: une grille en 2D.
: returntype: grille(list)
'''
    largeure = (len(grille[0]))
    longueur = (len(grille))
    for j in range(largeure):
        print('+---+'+'---+'*(longueur-1))
        for i in range(longueur):
           print('|',grille[i][j],end=" ")
        print('|',"")
    print('+---+'+'---+'*(longueur-1))

def principale(hauteur, largeur, nombre_requins, nombre_thons, duree_gestation_requins, duree_gestation_thons, energie_requins, nombre_tours):
    '''Fonction principale qui affiche l'evolution de la grille de jeu au fur et à mesure des n tours choisis par l'utilisateur.
: param: la hauteur de la grille,
         la largeur de la grille,
         le nombre de requins,
         le nombre de thons,
         la durée de gestation des requins,
         la durée de gestation des thons,
         l'énergie des requins,
         le nombre de tours.
: paramtype: hauteur(int), largeur(int), nombre_requins(int), nombre_thons(int), duree_gestation_requins(int),
             duree_gestation_thons(int), energie_requins(int), nombre_tours(int)
: return: l'évolution de la grille de jeu au bout de n tours
: returntype: grille(list)
'''
    aa = création_grille_jeu(largeure, hauteur, nombre_requins, nombre_thons, duree_gestation_requins, energie_requins, duree_gestation_thons)
    compteur = 0
    nbrequins = 0
    nbthons = 0
    affichage(aa)
    liste_x = []
    liste_y = []
    for i in range(nombre_tours):
        nb_elements_1 = len(grille_jeu)
        a = 0
        while a < nb_elements_1:
            nb_elements_2 = len(grille_jeu[a])    
            b = 0
            while b < nb_elements_2:
                identite = aa[a][b]
                itinéraire = contenu_cases(grille_jeu, ordonnee, abscisse, duree_gestation_requins, duree_gestation_thons, energie_requins)
                if identite == 'R':
                    nbrequins = nbrequins + 1
                elif identite == 'T':
                    nbthons = nbthons + 1
                case_suivante = itinéraire[1][1]
                deplacement(aa, a, b, case_suivante)
                compteur = compteur + 1
                if compteur == 100:
                    affichage(aa)
                    compteur = 0
            b = b+1
        a = a + 1
        liste_x.append(nbrequins)
        liste_y.append(nbthons)
        energie_requins = energie_requins - 1
        duree_gestation_thons = duree_gestation_thons - 1
        duree_gestation_requins = duree_gestation_requins - 1

    data_x = liste_x
    data_y = liste_y
    pylab.plot(data_x, data_y)
    pylab.title('coubres du nombre de requins et de thons au fur et à mesure des tours')