from time import sleep
from random import choices
from copy import copy

def creer_grille(h,v):
    '''Fonction qui créer une grille en fonction de la longueure verticale et horizontale que l'utilisateur choisis.
: param: h est la longeure horizontale de la grille,
         v est la longueure verticale de la grille.
: paramtype: h(int), v(int).
: return: renvois une grille avec les dimensions choisies.
: returntype: liste(int)
'''
    return [[0 for i in range(h)]for j in range(v)]

def hauteur_grille(creer_grille):
    '''Fonction qui renvoie le nombre de cases verticalement.
: param:une grille du jeu de la vier
: paramtype: int
: return: le nombre de case dans la grille en hauteur
: returntype: int
'''
    return len(creer_grille)

def largeure_grille(creer_grille):
    '''Fonction qui renvoie le nombre de case horizontallement d'une grille.
: param: une grille du jeu de la vie.
: paramtype: int
: return: le nombre de cases horizontallement d'une grille.
: returntype: int
'''
    return len(creer_grille[0])

def creer_grille_aléatoire(abscice,ordonée,p):
    '''Fonction qui renvoie une grille remplie aléatoirement de cellules.
: param: largeure de la grille,
         hauteur de la grille,
         la probapilité p qu'une case de la grille ai une cellule.
: paramtype: largeure(int), hauteur(int), probabilité p(random)
: return: grille remplie de cellules, ou non.
: returntype: list(int)
'''
    return [[choices([0,1],[1-p,p]) for i in range(abscice)]for j in range(ordonée)]

def voisins_case(grille,x,y):
    '''Fonction qui renvoie une liste contenant la valeur des cases voisines de la case donnée en paramètre.
: param: grille completée,
         abscise choisis,
         ordonnée chosis,
: paramtype: liste(int), x(int), y(int)
: return: liste de la valeur des cases voisines
: returntype: liste(int)
'''
    liste_voisins = []
    if (x == 0 or x == len(grille)-1) and (y == len(grille[0])-1 or y == 0):
        return voisins_coins(grille,x,y)
    elif (x == 0 or x == len(grille)-1 or 0<x<len(grille)-1) and (y == 0 or y == len(grille[0])-1 or 0<y<len(grille[0])-1) and x!=y:
        return voisins_cotés(grille,x,y)
    elif (x>0 or x<len(grille)-1) and (y>0 or y<len(grille[0])-1):
        return voisins_partout(grille,x,y)

def voisins_coins(grille,x,y):
    '''Fonction qui donne le contenus des cases voisnes uniquement si la case choisie est un coin.
: param: grille completée,
         abscise choisis,
         ordonnée chosis,
: paramtype: liste(int), x(int), y(int)
: return: liste de la valeur des cases voisines
: returntype: liste(int)
'''
    liste_voisins = []
    if x == 0 and y == 0:
        a = grille[1][0]
        b = grille[1][1]
        c = grille[0][1]
        liste_voisins.extend([a,b,c])
    elif x == 0 and y == len(grille[0])-1:
        a = grille[0][len(grille[0])-2]
        b = grille[1][len(grille[0])-2]
        c = grille[1][len(grille[0])-1]
        liste_voisins.extend([a,b,c])
    elif x == len(grille)-1 and y==len(grille[0])-1:
        a = grille[len(grille)-2][len(grille[0])-1]
        b = grille[len(grille)-2][len(grille[0])-2]
        c = grille[len(grille)-1][len(grille[0])-2]
        liste_voisins.extend([a,b,c])
    else:
        a =grille[len(grille)-2][0]
        b =grille[len(grille)-2][1]
        c =grille[len(grille)-1][1]
        liste_voisins.extend([a,b,c])
    return liste_voisins

def voisins_cotés(grille,x,y):
    '''Fonction qui donne le contenus des cases voisnes uniquement si la case choisie se situe sur un coté.
: param: grille completée,
         abscise choisis,
         ordonnée chosis,
: paramtype: liste(int), x(int), y(int)
: return: liste de la valeur des cases voisines
: returntype: liste(int)
'''
    y_bis = y-1
    x_bis = x-1
    liste_voisins = []
    if x == 0 and y>0 and y<len(grille[0])-1:
        a = grille[x][y_bis]
        b = grille[x+1][y_bis]
        c = grille[x+1][y]
        d = grille[x+1][y+1]
        e = grille[x][y+1]
        liste_voisins.extend([a,b,c,d,e])
    elif x == 2 and y>0 and y<len(grille[0])-1:
        a = grille[x][y_bis]
        b = grille[x_bis][y_bis]
        c = grille[x_bis][y]
        d = grille[x_bis][y+1]
        e = grille[x][y+1]
        liste_voisins.append([a,b,c,d,e])
    elif x>0 and x<len(grille)-1 and y == 0:
        a = grille[x_bis][y]
        b = grille[x_bis][y+1]
        c = grille[x][y+1]
        d = grille[x+1][y+1]
        e = grille[x+1][y]
        liste_voisins.extend([a,b,c,d,e])
    else:
        a = grille[x_bis][y]
        b = grille[x_bis][y_bis]
        c = grille[x][y_bis]
        d = grille[x+1][y_bis]
        e = grille[x+1][y]
        liste_voisins.extend([a,b,c,d,e])
    return liste_voisins

def voisins_partout(grille,x,y):
    '''Fonction qui donne le contenus des cases voisnes uniquement si la case choisie se situe dans un endroit entouré d'autres cases.
: param: grille completée,
         abscise choisis,
         ordonnée chosis,
: paramtype: liste(int), x(int), y(int)
: return: liste de la valeur des cases voisines
: returntype: liste(int)
'''
    liste_voisins = []
    x_bis = x-1
    y_bis=y-1
    a = grille[x_bis][y_bis]
    b = grille[x_bis][y]
    c = grille[x_bis][y+1]
    d = grille[x][y+1]
    e = grille[x+1][y+1]
    f = grille[x+1][y]
    g = grille[x+1][y_bis]
    h = grille[x][y_bis]
    liste_voisins.extend([a,b,c,d,e,f,g,h])
    return liste_voisins

def nb_cellule_voisin(grille,x,y):
    '''Fonction qui renvoie le nombre de cellules vivantes (1) dans les cases voisines de la case passée en paramètre.
: param: grille completée,
         abscise choisis,
         ordonnée chosis,
: paramtype: liste(int), x(int), y(int)
: return: le nombre de cellule dans les cases voisines
: returntype: int
'''
    acc = 0
    a = voisins_case(grille,x,y)
    i = 0
    for valeur in a:
        if valeur==1:
            acc = acc + 1
        else:
            acc = acc + 0
    return acc

def afficher_grille(grille):
    '''Fonction qui sert à afficher une grille de manière plus claire.
: param: une grille complétée
: paramtype: int
: return: une grille mieux organisée
: returntype:
'''
    nb_elements_1 = len(grille)
    a=0
    while a < nb_elements_1:
        nb_elements_2 = len(grille[a])    
        b = 0
        while b < nb_elements_2:
            if grille[a][b] == 0:
                grille[a][b] = '_'
            else:
                grille[a][b] = 'O'
            b = b + 1
        a = a + 1
    for j in range(len(grille)):
        for i in range(len(grille[0])):
            print(grille[j][i], end=" ")
        print("")
def generation_suivante(grille):
    '''Fonction qui renvoie la grille de la generation suivante à partir de celle passé en paramètre.
: param: grille complétée
: paramtype: int
: return: nouvelle grille
: returntype: int
'''
    nb_elements_1 = len(grille)
    a=0
    grille_bis = copy(grille)
    while a < nb_elements_1:
        nb_elements_2 = len(grille[0])    
        b = 0
        while b < nb_elements_2:
            c = nb_cellule_voisin(grille_bis,a,b)
            if grille[a][b] == 1:
                if c>3 or c<2: 
                    grille[a][b] = 0
            else:
                if c==3:
                    grille[a][b] = 1
            b = b + 1
        a = a + 1
    return grille

    

def evolution_n_generation(grille,n):
    '''Fonction qui affiche l'évolution de la grille au fil de n générations.
: pram: une grille complétée,
        n qui symbolise un nombre de génération
: paramtype: gille(int), n(int)
: return: les grilles évoluées au fil des générations
: returntype:
'''
    a = grille
    print(a)
    while n!=0:
        sleep(1)
        nouvelle_grille = generation_suivante(a)
        a = nouvelle_grille
        n = n - 1
        print (a)  