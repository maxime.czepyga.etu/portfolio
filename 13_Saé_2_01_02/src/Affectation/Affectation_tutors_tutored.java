package Affectation;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.ulille.but.sae2_02.graphes.Arete;
import fr.ulille.but.sae2_02.graphes.CalculAffectation;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;

public class Affectation_tutors_tutored {
	
	//Arguments
	private ArrayList<Tutors> tutors;
	private ArrayList<Tutored> tutored;
	static final int DEFAULTSCORE = 10;
	private Teacher teacher;
	private ArrayList<int []> manualAddingStudent = new ArrayList<int []>();
	
	//constructor 1
	public Affectation_tutors_tutored(ArrayList<Tutors> tutors, ArrayList<Tutored> tutored, Teacher teacher) {
		this.tutors = tutors;
		this.tutored = tutored;
		this.teacher = teacher;
	}
	
	//constructor 3
	public Affectation_tutors_tutored() {
		this(new ArrayList<Tutors>(), new ArrayList<Tutored>(), new Teacher());
	}
	
	//accessors
	public ArrayList<Tutors> getTutors() { return this.tutors; }
	public ArrayList<Tutored> getTutored() { return this.tutored; }
	public Teacher getTeacher() { return this.teacher; }
	public ArrayList<int []> getManualAddingStudent() { return this.manualAddingStudent; }
	
	
	
	//teacherOpinion
	public void deleteStudent() {
		boolean teacherOpinion = this.teacher.teacherOpinion(this.getTutors(), this.getTutored());
		if (teacherOpinion) {
			for (int i=0; i<this.getTutored().size(); i++) {
				if (this.isIn(this.getTutored().get(i))) this.tutored.remove(i);
			}
			for (int j=0; j<this.getTutors().size(); j++) {
				if (this.isIn(this.getTutors().get(j))) this.tutors.remove(j);
			}
		}
	}
	
	public boolean isIn(Tutors t) {
		for (int i=0; i<this.getTeacher().getDeleteStudent().size(); i++) {
			if (this.getTeacher().getDeleteStudent().get(i).equals(t)) return true;
		}
		return false;
	}
	
	public boolean isIn(Tutored t) {
		for (int i=0; i<this.getTeacher().getDeleteStudent().size(); i++) {
			if (this.getTeacher().getDeleteStudent().get(i).equals(t)) return true;
		}
		return false;
	}
	
	
	//Manual adding
	public void manualAdding() {
		Scanner answer1 = new Scanner(System.in);
		String stranswer1 = answer1.nextLine();
		if (stranswer1.equals("y")) {
			System.out.println("Les tuteurs :"+this.getTutors());
			System.out.println("Les tutor�s :"+this.getTutored());
			System.out.println("Veuillez nous donner l'indice du tuteur:");
			Scanner answer2 = new Scanner(System.in);
			String stranswer2 = answer1.nextLine();
			int indexTutor = Integer.parseInt(stranswer2, 10);
			System.out.println("Veuillez nous donner l'indice du tutor�:");
			Scanner answer3 = new Scanner(System.in);
			String stranswer3 = answer1.nextLine();
			int indexTutored = Integer.parseInt(stranswer3, 10);
			this.getManualAddingStudent().add(new int[] {indexTutor, indexTutored});
		}
	}
	
	public void addFictiveStudent() {
		if (this.getTutors().size()>this.getTutored().size()) {
			for (int i=0; i<this.getTutors().size()-this.getTutored().size(); i++) {
				this.tutored.add(new Tutored(true));
			}
		} else if (this.getTutors().size()<this.getTutored().size()) {
			for (int i=0; i<this.getTutored().size()-this.getTutors().size(); i++) {
				this.tutors.add(new Tutors(true));
			}
		}
	}
	
	
	// adding Vertices
	public void addingVertices(GrapheNonOrienteValue<String> g) {
		//le max size ici n'est pas tr�s utile puissequ'on va v�rifer que tutors et tutored 
		//font la m�me taille avant de les appeler dans le m�thode
		for (int i=0; i<this.getTutors().size(); i++) {
			g.ajouterSommet(this.getTutors().get(i).getFirstname());
			g.ajouterSommet(this.getTutored().get(i).getFirstname());
		}
	}
	
	//the affectation of weight
	public void weightAffectation (GrapheNonOrienteValue<String> g) {
		for (int i=0; i<this.getTutors().size(); i++) {
			for (int j=0; j<this.getTutored().size(); j++) {
				if (isInManualAdding(i,j)) {
					g.ajouterArete(this.getTutors().get(i).getFirstname(), 
							   	   this.getTutored().get(j).getFirstname(),
							   	   Integer.MIN_VALUE);
				} else {
					g.ajouterArete(this.getTutors().get(i).getFirstname(), 
								   this.getTutored().get(j).getFirstname(), 
								   Affectation_tutors_tutored.DEFAULTSCORE + 
								   this.weightValue(this.getTutors().get(i), this.getTutored().get(j)));
				}
							   
			}
		}
	}
	
	public boolean isInManualAdding(int a, int b) {
		for (int i=0; i<this.getManualAddingStudent().size(); i++) {
			if (a==this.getManualAddingStudent().get(i)[0] && b==this.getManualAddingStudent().get(i)[1]) return true;
		}
		return false;
	}
	
	
	private int weightValue(Tutors tutors, Tutored tutored) {
		int result = 0;
		result+= 10-tutors.getNote();
		if (tutors.getStudyYear() == 3) result-=5; 
		else if(tutors.getStudyYear() == 2) result+=5;
		
		result+= tutored.getNote()-10;
		result+= tutored.getMotivation()*(-2.5);
		
		return result;
	}
	
	// the first set
	public List<String> firstSet( List<String> p1) {
		for (int i=0; i<tutors.size(); i++) {
			p1.add(tutors.get(i).getFirstname());
		}
		return p1;
	}
	
	//the second set 
	public List<String> secondSet(List<String> p2) {
		for (int i=0; i<tutored.size(); i++) {
			p2.add(tutored.get(i).getFirstname());
		}
		return p2;
	}
	
	public String displayAffectation(CalculAffectation<String> cal) {
		String res = "";
		for (int i=0; i<this.getTutors().size(); i++) {
			if (!cal.getAffectation().get(i).getExtremite1().equals("UnknowFirstName") &&
					!cal.getAffectation().get(i).getExtremite2().equals("UnknowFirstName")) {
				res+="["+cal.getAffectation().get(i).getExtremite1()+","+cal.getAffectation().get(i).getExtremite2()+"]\n";
			}
		}
		return res;
	}
}
