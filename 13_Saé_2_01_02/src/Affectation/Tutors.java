package Affectation;

import java.util.Random;
import java.util.random.RandomGenerator;

public class Tutors extends Person{
	
	private String name;
	private String firstname;
	private double note;
	private final int STUDYYEAR;
	private final boolean FICTIVESTUDENT;
	static int student = 0;
	private int numberFictiveStudent = 0;
	
	
	public Tutors(String name, String firstname, int studyyear, double note, boolean fictivestudent) {
		this.name = name;
		this.firstname = firstname;
		this.STUDYYEAR = studyyear;
		this.note = note;
		this.FICTIVESTUDENT = fictivestudent;
	}
	
	public Tutors(String name, String firstname, int studyyear, double note) {
		this(name, firstname, studyyear, note, false);
	}
	
	public Tutors(boolean fictiveStudent) {
		this("UnknowName", "UnknowFirstName", 2, Integer.MIN_VALUE, fictiveStudent);
		this.numberFictiveStudent = Tutors.student+1;
		Tutors.student+=1;
	}

	public String getName() { return this.name; }
	public String getFirstname() { return this.firstname; }
	public double getNote() { return this.note; }
	public int getStudyYear() { return this.STUDYYEAR; }
	public boolean getFictiveStudent() { return this.FICTIVESTUDENT; }
	
	public String toString() {
		return this.getName()+" "+this.getFirstname();
	}
}
