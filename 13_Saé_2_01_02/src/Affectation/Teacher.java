package Affectation;

import java.util.ArrayList;

public class Teacher{
	
	private String name;
	private String firstname;
	private ArrayList<Person> deleteStudent;
	
	
	public Teacher(String name, String firstname, ArrayList<Person> deleteStudent) {
		this.name = name;
		this.firstname = firstname;
		this.deleteStudent = deleteStudent;
	}
	
	public Teacher(String name, String firstname) {  
		this(name, firstname, new ArrayList<Person>()); 
	}
	
	public Teacher() { 
		this("UnknowName", "UnknowFirstname");
	}
	
	public String getName() { return this.name; }
	public String getFirstname() { return this.firstname; }
	public ArrayList<Person> getDeleteStudent() { return this.deleteStudent; }
	
	public void setDeleteStudent(Person p) { this.deleteStudent.add(p); }
	
	public boolean manualAdding() { 
		return true;
	}
	
	public boolean teacherOpinion(ArrayList<Tutors> tutors, ArrayList<Tutored> tutored) {
		boolean result = false;
		for (int i=0; i<tutors.size(); i++) {
			if (tutors.get(i).getNote()<10) this.setDeleteStudent((Person) tutors.get(i));
			if (!result) result = true;
		}
		for (int j=0; j<tutored.size(); j++) {
			if (tutored.get(j).getNote()>15) this.setDeleteStudent((Person) tutored.get(j));
			if (!result) result = true;
		}
		return result;
	}
	
	public String toString() { return "Mr/Mme ["+this.getFirstname()+","+this.getName()+"]"; }
	public String displayOpinion() {
		String result = "";
		for (int i=0; i<this.getDeleteStudent().size(); i++) result+= this.getDeleteStudent().get(i)+"\n";
		return result;
	}
}
