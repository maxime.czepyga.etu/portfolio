package Affectation;

import fr.ulille.but.sae2_02.graphes.CalculAffectation;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;
import java.util.ArrayList;
import java.util.List;

public class UseAffectation {
	
	private static void script() {
		ArrayList<Tutors> tutors = new ArrayList<Tutors>();
		ArrayList<Tutored> tutored = new ArrayList<Tutored>();
		List<String> p1 = new ArrayList<String>();
		List<String> p2 = new ArrayList<String>();

	
		tutors.add(new Tutors("Czepyga", "Maxime", 3, 13));
		tutors.add(new Tutors("Delzenne", "Milan", 3, 16.25));
		tutors.add(new Tutors("Larsonnier", "C�dric", 3, 12.5));
		tutors.add(new Tutors("Dubois", "Alice", 2, 9.25));
		tutors.add(new Tutors("Gamet", "Elo�se", 2, 14.25));
		tutors.add(new Tutors("Muttor", "L�o", 2, 15));
		
		tutored.add(new Tutored("Dumont", "Benoit", -2, 13));
		tutored.add(new Tutored("Levort", "Jeanne", 1, 16.25));
		tutored.add(new Tutored("Hennart", "Victor", 2, 12.5));
		tutored.add(new Tutored("Kowalikovsky", "Z�lie", 0, 9.25));
		tutored.add(new Tutored("Lagache", "Chlo�", -2, 14.25));
		tutored.add(new Tutored("Yesse", "Jack", -1, 15));
		
		Affectation_tutors_tutored affectation = new Affectation_tutors_tutored(tutors, tutored, new Teacher("Antoine", "Nongaillard"));
		
		affectation.deleteStudent();
		
		System.out.println("Est-ce que le professeur veut affecter manuelement un tuteur � un tutor�? [y/n]");
		affectation.manualAdding();
		affectation.addFictiveStudent();
		
		GrapheNonOrienteValue<String> g = new GrapheNonOrienteValue<String>();
		affectation.addingVertices(g);
		affectation.weightAffectation(g);
		affectation.firstSet(p1);
		affectation.secondSet(p2);
		
		CalculAffectation<String> cal = new CalculAffectation<String>(g, p1, p2);
		//System.out.println(cal.getAffectation());
		//System.out.println(cal.getCout());
		System.out.println(affectation.displayAffectation(cal));
	}

	public static void main(String[] args) {
		UseAffectation.script();
	}
}
