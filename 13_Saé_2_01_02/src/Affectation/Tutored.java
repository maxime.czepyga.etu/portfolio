package Affectation;

import java.util.Random;

public class Tutored extends Person{

	private String name;
	private String firstname;
	private double note;
	//the motivation is an integer between -2 and 2
	private final int MOTIVATION;
	private final boolean FICTIVESTUDENT;
	static int student = 0;
	private int numberFictiveStudent = 0;
	
	public Tutored(String name, String firstname, int motivation, double note, boolean fictivestudent) {
		this.name = name;
		this.firstname = firstname;
		this.MOTIVATION = motivation;
		this.note = note;
		this.FICTIVESTUDENT = fictivestudent;
	}
	
	public Tutored(String name, String firstname, int motivation, double note) {
		this(name, firstname, motivation, note, false);
	}
	
	public Tutored(boolean fictiveStudent) {
		this("UnknowName", "UnknowFirstName", -2, Integer.MAX_VALUE, fictiveStudent);
		this.numberFictiveStudent = Tutored.student+1;
		Tutored.student+=1;
	}
	
	public String getName() { return this.name; }
	public String getFirstname() { return this.firstname; }
	public double getNote() { return this.note; }
	public int getMotivation() { return this.MOTIVATION; }
	
	public String toString() { return this.getName()+" "+this.getFirstname(); }
	
}
