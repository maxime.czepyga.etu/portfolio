class enigma extends Program {

    // CONSTANTES DE LA MACHINE
    String ALPHABET ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String ROTOR1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
    String ROTOR2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; 
    String ROTOR3 = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
    String ROTOR4 = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
    String ROTOR5 = "VZBRGITYUPSDNHLXAWMJQOFECK";
    String REFLECTEURA = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
    String REFLECTEURB = "RDOBJNTKVEHMLFCWZAXGYIPSUQ";

    void testLettreEnNombre(){
	assertEquals(0, lettreEnNombre('A'));
	assertEquals(12, lettreEnNombre('M'));
	assertEquals(7, lettreEnNombre('H'));
    }

    void testNombreEnLettre(){
	assertEquals('A', nombreEnLettre(0));
	assertEquals('M', nombreEnLettre(12));
	assertEquals('H', nombreEnLettre(7));
    }

    void testChoixRotor(){
	assertEquals("EKMFLGDQVZNTOWYHXUSPAIBRCJ", choixRotor(1));
	assertEquals("AJDKSIRUXBLHWTMCQGZNPYFVOE", choixRotor(2));
	assertEquals("BDFHJLCPRTXVZNYEIWGAKMUSQO", choixRotor(3));
	assertEquals("ESOVPZJAYQUIRHXLNFTGKDCMWB", choixRotor(4));
	assertEquals("VZBRGITYUPSDNHLXAWMJQOFECK", choixRotor(5));
    }

    void testChoixReflecteur(){
	assertEquals("YRUHQSLDPXNGOKMIEBFZCWVJAT", choixReflecteur('A'));
	assertEquals("RDOBJNTKVEHMLFCWZAXGYIPSUQ", choixReflecteur('B'));
    }

    void testDecalageUnRang(){
	assertEquals("BCDEFGHIJKLMNOPQRSTUVWXYZA", decalageUnRang("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
	assertEquals("FHUQSMDVHNQOIVHZI", decalageUnRang("IFHUQSMDVHNQOIVHZ"));
    }

    void testPositionInitialeRotor(){
	assertEquals("DEFGHIJKLMNOPQRSTUVWXYZABC", positionInitialeRotor("ABCDEFGHIJKLMNOPQRSTUVWXYZ",3));
	assertEquals("SMDVHNQOIVHZIFHUQ", positionInitialeRotor("IFHUQSMDVHNQOIVHZ",5));
	assertEquals("NPROJETAVECHOUZAYFAMAXIMEE", positionInitialeRotor("MAXIMEENPROJETAVECHOUZAYFA",7));
    }

    void testIndiceLettre(){
	assertEquals(0, indiceLettre('A',"ABCDE"));
	assertEquals(1, indiceLettre('B',"ABCDE"));
	assertEquals(2, indiceLettre('C',"ABCDE"));
	assertEquals(3, indiceLettre('D',"ABCDE"));
	assertEquals(4, indiceLettre('E',"ABCDE"));
	assertEquals(-1, indiceLettre('F',"ABCDE"));
	assertEquals(3, indiceLettre('F',ROTOR1));
    }

    void testValeurApresCablageDeDepart(){
	assertEquals('O', valeurApresCablageDeDepart('H',"AVDEHOJKLSXQ"));
	assertEquals('B', valeurApresCablageDeDepart('A',"ABCDEFGHIJKL"));
	assertEquals('A', valeurApresCablageDeDepart('B',"ABCDEFGHIJKL"));
	assertEquals('L', valeurApresCablageDeDepart('K',"ABCDEFGHIJKL"));
	assertEquals('C', valeurApresCablageDeDepart('D',"ABCDEFGHIJKL"));
	assertEquals('M', valeurApresCablageDeDepart('M',"ABCDEFGHIJKL"));
    }

    void testPassageDansUnRotor(){
	assertEquals('E', passageDansUnRotor('A',ROTOR1));
	assertEquals('K', passageDansUnRotor('B',ROTOR1));
	assertEquals('J', passageDansUnRotor('Z',ROTOR1));
	assertEquals('S', passageDansUnRotor('E',"AJDKSIRUXBLHWTMCQGZNPYFVOE"));
    }

    void testPassageDansLeReflecteur(){
	assertEquals('Y', passageDansLeReflecteur('A',REFLECTEURA));
	assertEquals('R', passageDansLeReflecteur('B',REFLECTEURA));
	assertEquals('T', passageDansLeReflecteur('Z',"YRUHQSLDPXNGOKMIEBFZCWVJAT"));
    }

    void testinverseRotor(){
	assertEquals('A', inverseRotor('E',ROTOR1));
	assertEquals('B', inverseRotor('K',ROTOR1));
	assertEquals('Z', inverseRotor('J',ROTOR1));
	assertEquals('E', inverseRotor('S',"AJDKSIRUXBLHWTMCQGZNPYFVOE"));
    }

    void testEnMasjuscule(){
	assertEquals("ABCDEFGHIJ", enMajuscule("abcdefghij"));
	assertEquals("LMNOPQRST", enMajuscule("lmnOPQRST"));
	assertEquals("LMNOPQRST", enMajuscule("LMNOpqrst"));
	assertEquals("MAXIMEETHOUZAYFAENPROJET", enMajuscule("MaXiMeEtHoUzAyFaEnPrOjEt"));
	assertEquals("ABCDEFGHIJ", enMajuscule("abcdefghij"));
    }

    int lettreEnNombre(char lettre){
	return lettre-'A';
    }

    char nombreEnLettre(int nombre){
	return (char) (nombre+'A');
    }

    String choixRotor(int numeroRotor){
	String resultat = "";
	if (numeroRotor==1){
	    resultat = ROTOR1;
	} else if (numeroRotor == 2){
	    resultat = ROTOR2;
	} else if (numeroRotor == 3){
	    resultat = ROTOR3;
	} else if (numeroRotor == 4) {
	    resultat = ROTOR4;
	} else {
	    resultat = ROTOR5;
	}
	return resultat;
    }

    String choixReflecteur(char lettreReflecteur){
	String resultat = REFLECTEURB;
	if (lettreReflecteur=='A'){
	    resultat = REFLECTEURA;
	}
	return resultat;
    }

    String cablageInitial(){
	String resultat = "";
	for(int cpt=0; cpt<6; cpt++){
	    println("Une paire de lettres?");
	    String paire = readString();
	    resultat = resultat + paire;
	}
	return resultat;
    }
	
    void affichageCablageInitial(){
	println(cablageInitial());
    }

    String decalageUnRang(String rotor){
	return substring(rotor,1, length(rotor)) + substring(rotor,0,1);
    }

    String positionInitialeRotor(String rotor, int position){
	for (int cpt = 0; cpt<position; cpt = cpt + 1){
	    rotor = decalageUnRang(rotor);
	}
	return rotor;
    }

    int indiceLettre(char lettre, String cablage){
	int sortie = -1;
	for (int cpt=0; cpt<length(cablage); cpt=cpt+1 ){
	    if (lettreEnNombre(charAt(cablage,cpt)) == lettreEnNombre(lettre)){
		sortie = cpt;
	    }
	}
	return sortie;
    }

    char valeurApresCablageDeDepart(char lettre, String cablage){
	char resultat = lettre;
	for (int cpt=0; cpt<(length(cablage)-1); cpt=cpt+2){
	    if (charAt(cablage,cpt) == lettre){
		resultat = charAt(cablage,cpt+1);
	    } else if (charAt(cablage,cpt+1) == lettre){
		resultat = charAt(cablage,cpt);
	    }
	}
	return resultat;
    }

    char passageDansUnRotor(char lettre, String rotor){
	return charAt(rotor,lettreEnNombre(lettre));
    }

    char passageDansLeReflecteur(char lettre, String reflecteur){
	return charAt(reflecteur,lettreEnNombre(lettre));
    }

    char inverseRotor(char lettre, String rotor){
	int indice = indiceLettre(lettre,rotor);
	return charAt(ALPHABET,indice);
    }

    String enMajuscule(String message){
        String resultat = "";
	for (int cpt=0; cpt<length(message); cpt=cpt+1){
	    char lettre = charAt(message,cpt);
	    if (lettre>91){
		resultat = resultat + (char) (lettre-32);
	    } else if (lettre<91){
		resultat=resultat+lettre;
	    }
	}
	return resultat;	
    }

    void algorithm(){
	println(" ------------------------------------\n| Simulation d'une machine Enigma M3 |\n ------------------------------------");

	println("Quel type de configuration souhaitez-vous ? \n 1 : Configuration par défaut (Rotor 1 : III en position W, Rotor 2 : I en position D, Rotor 3 : V en position E, Réflecteur : B, Cablâge : AV - DE - HO - JK - LS - XQ, Message à décoder par défaut)\n 2 : Configuration personnalisée");
	int choix = readInt();
	while (choix<1 || choix>2){
	    println("Erreur de saisie, choix : 1 ou 2");
	    choix = readInt();
	}
	
	int rotor1,rotor2,rotor3;
	String R1,R2,R3;
	char position1,position2,position3;
	int decalage1,decalage2,decalage3;
	char choixRef;
	String refl;
	String cables;
	
	String message="";
	
	if (choix == 1){
	    rotor1 = 3;
	    rotor2 = 1;
	    rotor3 = 5;
	    R1=choixRotor(rotor1);
	    R2=choixRotor(rotor2);
	    R3=choixRotor(rotor3);
	    

	    position1 = 'W';
	    position2 = 'D';
	    position3 = 'E';
	    decalage1 = indiceLettre(position1,R1);
	    R1 = positionInitialeRotor(R1,decalage1);
	    decalage2 = indiceLettre(position2,R2);
	    R2 = positionInitialeRotor(R2,decalage2);
	    decalage3 = indiceLettre(position3,R3);
	    R3 = positionInitialeRotor(R3,decalage3);
	    
	    choixRef = 'B';
	    refl = choixReflecteur(choixRef);

	    cables = "AVDEHOJKLSXQ";

	    message = "AKBAOKETGPVYHGWBSGSVUDTZEBNOXGFOBVYOJVTWFPIKC";
	}
	else{
	    println("Entrez le numéro du premier rotor choisi (1, 2, 3, 4, 5)");
	    rotor1 = readInt();
	    println("Entrez le numéro du deuxième rotor choisi");
	    rotor2 = readInt();
	    println("Entrez le numéro du troisième rotor choisi");
	    rotor3 = readInt();
	    
	    R1=choixRotor(rotor1);
	    R2=choixRotor(rotor2);
	    R3=choixRotor(rotor3);

	    println("Entrez la position initiale du premier rotor choisi (A à Z)");
	    position1 = readChar();
	    println("Entrez la position initiale du deuxième rotor choisi (A à Z)");
	    position2 = readChar();
	    println("Entrez la position initiale du troisième rotor choisi (A à Z)");
	    position3 = readChar();
	    
	    decalage1 = indiceLettre(position1,R1);
	    R1 = positionInitialeRotor(R1,decalage1);
	    decalage2 = indiceLettre(position2,R2);
	    R2 = positionInitialeRotor(R2,decalage2);
	    decalage3 = indiceLettre(position3,R3);
	    R3 = positionInitialeRotor(R3,decalage3);

	    println("Entrez la lettre du réflecteur choisi (A ou B)");
	    choixRef = readChar();
	    refl = choixReflecteur(choixRef);

	    cables = cablageInitial();

	    println("Entrez le message à coder :");
	    message = readString();
	    message = enMajuscule(message);
	}
	
	String messageDecode="";
	///////////////////
        char lettre;
	///////////////////
	for (int tour=0 ; tour < length(message) ; tour=tour+1){
	    lettre = charAt(message,tour);
	    lettre = valeurApresCablageDeDepart(lettre, cables);
	    lettre = passageDansUnRotor(lettre, R1);
	    lettre = passageDansUnRotor(lettre, R2);
	    lettre = passageDansUnRotor(lettre, R3);
	    lettre = passageDansLeReflecteur(lettre,refl);
	    lettre = inverseRotor(lettre,R3);
	    lettre = inverseRotor(lettre,R2);
	    lettre = inverseRotor(lettre,R1);
	    lettre = valeurApresCablageDeDepart(lettre,cables);
	    messageDecode = messageDecode + lettre;
	    R1 = decalageUnRang(R1);
	    if (tour==26){
		R2 = decalageUnRang(R2);
	    }
	    if (tour == (26*26)){
		R3 = decalageUnRang(R3);
	    }
	}
	if (choix == 2 ){
	    println("Le message codé est : \n" + messageDecode);
	} else {
	    println("Le message décodé est : \n" + messageDecode);
	}
    }	
}