#---------------------------------------------
#  Importation des modules
#---------------------------------------------

from random import random

#----------------------------------------------
# Fonctions d'affichage
#----------------------------------------------

def affiche_joueur(joueur_actuel):
    '''Affiche le nom du joueur à qui c'est le tour de jouer'''
    print('A ' + (joueur_actuel) + ' de jouer')


def affiche_situation(situation):
    '''Affiche les allumettes'''
    res = ''
    nb_allumette_par_ligne = 1
    while situation >= 0:
        res += ' | ' * nb_allumette_par_ligne + '\n'
        situation -= nb_allumette_par_ligne
        nb_allumette_par_ligne += 1
    print('\n' + res)


def affiche_choix(choix, joueur_actuel) :
    '''Affiche le nombre d\\\'allumettes que le joueur a choisies de prendre'''
    print(str(joueur_actuel) + ' prend ' + str(choix) + ' allumettes')


def affiche_coups(coups_max):
    '''Affiche le nombre d'allumettes maximum que le joueur peut prendre'''
    if coups_max == 1:
        print('Vous ne pouvez choisir de ne prendre qu\'une seule allumette')
    elif coups_max == 2:
        print('Vous pouvez choisir de prendre 1 ou 2 allumettes')
    else :
        print('Vous pouvez choisir de prendre 1, 2 ou 3 allumettes')


def affiche_fin_partie(joueur_actuel):
    '''Affiche le nom du jouer qui a gagné la partie'''
    print('Partie terminée. \nGagnant : ' + str(joueur_actuel))


#----------------------------------------------
# Fonctions relatives aux joueurs
#-----------------------------------------------

def change_joueur(joueur_actuel, joueur1,  joueur2):
    '''Renvoie le joueur à qui ce n'est pas le tour de jouer'''
    if joueur_actuel == joueur1 :
        return joueur2
    else :
        return joueur1


def premier_joueur(joueur1, joueur2):
    '''Tire au sort le premier joueur à jouer'''
    nombre = random()
    if nombre < 0.5:
        return joueur1
    else :
        return joueur2
    
    
#----------------------------------------------
# Fonctions liées aux coups joués
#----------------------------------------------

def existe_coup(situation):
    '''Renvoie 'False' si il n'y a plus d'allumettes à choisir'''
    if situation == 0:
        return False
    else :
        return True


def coups_possibles(situation):
    '''Renvoie le nombre maximum d'allumettes pouvant être choisi par le joueur'''
    if situation >= 3:
        return 3
    elif situation >= 2:
        return 2
    else :
        return 1
    
def choix_coups(coups_max):
    '''Demande au joueur le nombre d'allumettes qu'il souhaite prendre.
    Celle ci est répétée jusqu'à ce que le joueur choisisse de prendre un nombre d'allumettes
    inférieur au coups_max'''
    nb_allumettes = 100
    
    while nb_allumettes > coups_max or nb_allumettes == 0:
        nb_allumettes = int(input('Combien d\'allumettes prenez-vous (max :' + str(coups_max) + ') ? '))
    
    return nb_allumettes

def msj_situation(choix, situation):
    '''Renvoie la nouvelle situation après que le joueur à jouer.'''
    return situation - choix


#---------------------------------------------
# Fonction principale
#---------------------------------------------

def jeu():
    '''Affiche les etapes complètes du jeu de nim et le gagnant en utilisant les fonctions précédentes'''
    situation = 16
    joueur_1 = input('Comment s\'appelle le joueur n°1 ? ')
    joueur_2 = input('Comment s\'appelle le  joueur n°2 ? ')
    joueur_actuel = premier_joueur(joueur_1, joueur_2)
    affiche_situation(situation)
    while existe_coup(situation) == True :
        if situation < 16 :
            joueur_actuel = change_joueur(joueur_actuel, joueur_1,  joueur_2)
        affiche_joueur(joueur_actuel)
        coups_max = coups_possibles(situation)
        affiche_coups(coups_max)
        choix = choix_coups(coups_max)
        situation = msj_situation(choix, situation)
        affiche_choix(choix, joueur_actuel)
        affiche_situation(situation)
    affiche_fin_partie(joueur_actuel)