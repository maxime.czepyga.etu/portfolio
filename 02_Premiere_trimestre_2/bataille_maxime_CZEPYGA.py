#---------------------------------------------
#  Importation des modules
#---------------------------------------------
from random import randint
from time import sleep


#----------------------------------------------
# Fonction création jeu de cartes 
#----------------------------------------------

def jeu_de_cartes() :
    """
    Fonction qui retourne une liste comportant un jeu de 52 cartes.
    """
    return ["As ♠", "2 ♠", "3 ♠", "4 ♠"
                    , "5 ♠", "6 ♠", "7 ♠", "8 ♠"
                    , "9 ♠", "10 ♠", "Valet ♠"
                    , "Dame ♠", "Roi ♠", "As ♣", "2 ♣"
                    , "3 ♣", "4 ♣", "5 ♣", "6 ♣"
                    , "7 ♣", "8 ♣", "9 ♣", "10 ♣"
                    , "Valet ♣", "Dame ♣", "Roi ♣"
                    , "As ♥", "2 ♥", "3 ♥", "4 ♥"
                    , "5 ♥", "6 ♥", "7 ♥", "8 ♥"
                    , "9 ♥", "10 ♥", "Valet ♥"
                    , "Dame ♥", "Roi ♥", "As ♦"
                    , "2 ♦", "3 ♦", "4 ♦"
                    , "5 ♦", "6 ♦", "7 ♦", "8 ♦"
                    , "9 ♦", "10 ♦", "Valet ♦"
                    , "Dame ♦", "Roi ♦"]

#----------------------------------------------
# Fonctions d'affichage
#----------------------------------------------

def afficher_carte(jeu1, jeu2):
    '''affiche les premières cartes des deux jeux'''
    print(f"{jeu1[0]} {jeu2[0]}")


def afficher_nombres_cartes(jeu1, jeu2, nom1, nom2):
    '''affiche le nombre de cartes dans les jeux des joueurs'''
    print(f"{nom1} a un paquet de {len(jeu1)} cartes.\n{nom2} a un paquet de {len(jeu2)} cartes.")


def afficher_vainqueur_tour(gagnant, nom1, nom2):
    '''affiche le gagnant du tour ou informe le joueur d'une nouvelle bataille'''
    if gagnant == 'joueur1':
        print(f"{nom1} a gagné le tour.\n")
    elif gagnant == 'joueur2':
        print(f"{nom2} a gagné le tour.\n")
    elif gagnant == 'bataille':
        print("Bataille !\n")
 
 
def afficher_vainqueur_jeu(gagnant, jeu1, jeu2, nom1, nom2):
    '''affiche le vainqueur de la partie'''
    if gagnant == 'joueur1':
        print(f"{nom1} a gagné la partie !")
    elif gagnant == 'joueur2':
        print(f"{nom2} a gagné la partie !")
    elif gagnant == 'bataille' and jeu1 == []:
        print(f"{nom2} a gagné la partie !")
    elif gagnant == 'bataille' and jeu2 == []:
        print(f"{nom1} a gagné la partie !")


#----------------------------------------------
# Fonctions relatives aux cartes des joueurs
#-----------------------------------------------

def valeur_carte(carte):
    '''renvoie la valeur de chaque carte'''
    if carte[0] == 'A':
        valeur = 14
    elif carte[0] == 'R':
        valeur = 13
    elif carte[0] == 'D':
        valeur = 12
    elif carte[0] == 'V':
        valeur = 11
    elif carte[0:2] == '10':
        valeur = 10
    else:
        valeur = int(carte[0])
    return valeur
        
        
def comparer_carte(carte_joueur1, carte_joueur2):
    '''compare les deux cartes d'un tour'''
    if valeur_carte(carte_joueur1) > valeur_carte(carte_joueur2):
        gagnant = 'joueur1'
    elif valeur_carte(carte_joueur1) < valeur_carte(carte_joueur2):
        gagnant = 'joueur2'
    elif valeur_carte(carte_joueur1) == valeur_carte(carte_joueur2):
        gagnant = 'bataille'
    return gagnant
   
   
#----------------------------------------------
# Fonctions relatives aux jeux des joueurs
#-----------------------------------------------

def distribution_jeu(jeu_complet, nombre_cartes):
    '''distribue nombre_cartes cartes dans un nouveau jeu'''
    nv_jeu = []
    for i in range(nombre_cartes):
        alea = randint(0, len(jeu_complet) - 1)
        carte_tiree = jeu_complet.pop(alea)
        nv_jeu.append(carte_tiree)
    return nv_jeu


def ordonner_jeu(jeu_vainqueur, jeu_perdant):
    '''réorganise les deux jeux après un tour simple (sans bataille)'''
    cartes_jouees = []
    cartes_jouees.append(jeu_vainqueur.pop(0))
    cartes_jouees.append(jeu_perdant.pop(0))
    jeu_vainqueur += cartes_jouees


def gestion_bataille(jeu_joueur1, jeu_joueur2, gagnant_bataille):
    '''gère les batailles et réorganise le jeu'''
    nb_bataille = 0
    while gagnant_bataille == 'bataille':
        nb_bataille += 1
        #Test pour savoir s'il y a assez de cartes pour la bataille
        if nb_bataille * 2 + 1 > len(jeu_joueur1):
            gagnant_bataille = 'joueur2'
            nb_range = len(jeu_joueur1)
        elif nb_bataille * 2 + 1 > len(jeu_joueur2):
            gagnant_bataille = 'joueur1'
            nb_range = len(jeu_joueur2)
        elif valeur_carte(jeu_joueur1[2 * nb_bataille]) > valeur_carte(jeu_joueur2[2 * nb_bataille]) :
            gagnant_bataille = 'joueur1'
            nb_range = nb_bataille * 2 + 1
        elif valeur_carte(jeu_joueur1[2 * nb_bataille]) < valeur_carte(jeu_joueur2[2 * nb_bataille]) :
            gagnant_bataille = 'joueur2'
            nb_range = nb_bataille * 2 + 1
    for i in range(nb_range):
        if gagnant_bataille == 'joueur1':
            ordonner_jeu(jeu_joueur1, jeu_joueur2)
        elif gagnant_bataille == 'joueur2':
            ordonner_jeu(jeu_joueur2, jeu_joueur1)


def gestion_tour(gagnant, jeu_joueur1, jeu_joueur2):
    '''gère un tour en fonction du gagnant'''
    if gagnant == 'joueur1':
        ordonner_jeu(jeu_joueur1, jeu_joueur2)
    elif gagnant == 'joueur2':
        ordonner_jeu(jeu_joueur2, jeu_joueur1)
    elif gagnant == 'bataille':
        gestion_bataille(jeu_joueur1, jeu_joueur2, gagnant)
  
  
#---------------------------------------------
# Fonction principale
#---------------------------------------------
   

def jeu():
    '''fonction principale du jeu'''
    jeu_complet = jeu_de_cartes()
    nom_joueur1 = input("Nom du joueur 1 : ")
    nom_joueur2 = input("Nom du joueur 2 : ")
    jeu_joueur1 = distribution_jeu(jeu_complet, 26)
    jeu_joueur2 = distribution_jeu(jeu_complet, 26)
    while jeu_joueur1 != [] and jeu_joueur2 != [] :
        vainqueur = comparer_carte(jeu_joueur1[0], jeu_joueur2[0])
        afficher_nombres_cartes(jeu_joueur1, jeu_joueur2, nom_joueur1, nom_joueur2)
        afficher_carte(jeu_joueur1, jeu_joueur2)
        afficher_vainqueur_tour(vainqueur, nom_joueur1, nom_joueur2)
        gestion_tour(vainqueur, jeu_joueur1, jeu_joueur2)
        sleep(0.5)
    afficher_vainqueur_jeu(vainqueur, jeu_joueur1, jeu_joueur2, nom_joueur1, nom_joueur2)