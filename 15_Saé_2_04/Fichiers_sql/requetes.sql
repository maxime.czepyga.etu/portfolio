/*Q1*/
SELECT n55, n54-(n56+n57) FROM import;

/*Q2*/
/*Cette requête n'affiche donc rien !*/
SELECT n55, n54-(n56+n57) FROM import WHERE n55 <> n54-(n56+n57);

/*Q3*/
SELECT n73, (n50::FLOAT/n45::FLOAT)*100 FROM import WHERE n45::FLOAT <> 0.0;

/*Q4*/
/* Prendre en compte les erreurs d'arrondi*/
SELECT n73, (n50::FLOAT/n45::FLOAT)*100 FROM import WHERE n45::FLOAT <> 0.0 AND ((n50::FLOAT/n45::FLOAT)*100 > n73::FLOAT+0.1 OR (n50::FLOAT/n45::FLOAT)*100 < n73::FLOAT-0.1 );

/*Q5*/
SELECT n76, (n70/n54::FLOAT)*100 FROM import WHERE n54::FLOAT<>0.0;

/*Q6*/
SELECT (Admis_issus_de_la_même_académie/Admis_néo_bacheliers::FLOAT)*100 FROM Admi WHERE Admis_néo_bacheliers::FLOAT<>0.0;

/*Q7*/
SELECT n81, (n59/n54::FLOAT)*100 FROM import WHERE n54<>0.0;

/*Q8*/
SELECT (Admis_néo_bacheliers_sans_information_sur_la_mention_au_bac / Admis_néo_bacheliers::FLOAT)*100 FROM Admi WHERE Admis_néo_bacheliers::FLOAT<>0.0;

