/*les drops*/
/*DROP TABLE import;*/
DROP TABLE Classement;
DROP TABLE Boursier;
DROP TABLE Admi;
DROP TABLE Candidat;
DROP TABLE Former;
DROP TABLE Formation;
DROP TABLE Etablissement;
DROP TABLE Departement;

/*import*/
/*CREATE TABLE import (n0 INTEGER, n1 CHAR(200), n2 CHAR(10), n3 CHAR(150), n4 CHAR(10), 
                                         n5 CHAR(30), n6 CHAR(50), n7 CHAR(20), n8 CHAR(50), n9 CHAR(25), 
                                         n10 CHAR(20), n11 CHAR(500), n12 CHAR(100), n13 CHAR(400), n14 CHAR(500), 
                                         n15 CHAR(25), n16 INTEGER, n17 INTEGER, n18 INTEGER, n19 INTEGER, 
                                         n20 CHAR(10), n21 INTEGER, n22 INTEGER, n23 INTEGER, n24 INTEGER, 
                                         n25 INTEGER, n26 INTEGER, n27 INTEGER, n28 INTEGER, n29 INTEGER, 
                                         n30 INTEGER, n31 INTEGER, n32 INTEGER, n33 INTEGER, n34 INTEGER, 
                                         n35 CHAR(10), n36 CHAR(10), n37 INTEGER, n38 INTEGER, n39 INTEGER, 
                                         n40 INTEGER, n41 INTEGER, n42 INTEGER, n43 INTEGER, n44 INTEGER, 
                                         n45 CHAR(15), n46 INTEGER, n47 INTEGER, n48 INTEGER, n49 FLOAT, 
                                         n50 CHAR(10), n51 FLOAT, n52 CHAR(10), n53 INTEGER, n54 INTEGER, 
                                         n55 INTEGER, n56 INTEGER, n57 INTEGER, n58 INTEGER, n59 INTEGER, 
                                         n60 INTEGER, n61 INTEGER, n62 INTEGER, n63 INTEGER, n64 FLOAT, 
                                         n65 INTEGER, n66 INTEGER, n67 INTEGER, n68 CHAR(10), n69 CHAR(10), 
                                         n70 INTEGER, n71 INTEGER, n72 CHAR(15), n73 CHAR(15), n74 CHAR(15), 
                                         n75 CHAR(15), n76 CHAR(15), n77 CHAR(15), n78 CHAR(20), n79 CHAR(15), 
                                         n80 CHAR(15), n81 CHAR(50), n82 CHAR(20), n83 CHAR(50), n84 CHAR(15), 
                                         n85 CHAR(15), n86 CHAR(15), n87 CHAR(15), n88 CHAR(15), n89 CHAR(20), 
                                         n90 CHAR(20), n91 CHAR(50), n92 CHAR(50), n93 FLOAT, n94 FLOAT, 
                                         n95 FLOAT, n96 FLOAT, n97 FLOAT, n98 FLOAT, n99 FLOAT, n100 CHAR(100), 
                                         n101 CHAR(15), n102 CHAR(100), n103 CHAR(10), n104 CHAR(100), n105 CHAR(10), 
                                         n106 CHAR(100), n107 CHAR(15), n108 CHAR(30), n109 INTEGER, n110 CHAR(500), 
                                         n111 CHAR(400), n112 CHAR(100), n113 CHAR(15), n114 CHAR(15), n115 CHAR(15), 
                                         n116 CHAR(5), n117 CHAR(5));

\copy import from index.html?format=csv delimiter ';';
*/

/*Departement*/
CREATE TABLE Departement (Code_departemental CHAR(10) PRIMARY KEY, Departement_nom CHAR(30));

INSERT INTO Departement SELECT DISTINCT n4, n5 FROM import;

/*Formation*/
CREATE TABLE Formation(Session INTEGER, Sélectivité CHAR(25), Filière_de_formation_très_agrégée CHAR(20), 
                       Filière_de_formation_détaillée CHAR(500), Filière_de_formation CHAR(100), 
                       Filière_de_formation_détaillée_bis CHAR(400), Filière_de_formation_très_détaillée CHAR(500), 
                       Coordonnées_GPS_de_la_formation CHAR(25), list_com CHAR(100), 
                       Taux_d_accès_des_candidats_ayant_postulé_à_la_formation CHAR(15), COD_AFF_FORM INTEGER PRIMARY KEY, 
                       LIB_FOR_VOE_INS CHAR(500), detail_forma2 CHAR(400), Lien_de_la_formation_sur_la_plateforme_Parcoursup CHAR(100));

INSERT INTO Formation SELECT n0, n9, n10, n11, n12, n13, n14, n15, n106, n107, n109, n110, n111, n112 FROM import;

/*Etablissement*/
CREATE TABLE Etablissement (Etablissement_nom CHAR(150), Statut CHAR(200), Code_UAI CHAR(10) PRIMARY KEY, Code_departemental CHAR(10), 
                            Region CHAR(50), Académie CHAR(40), Commune CHAR(50));

ALTER TABLE Etablissement ADD CONSTRAINT etablissement_fk FOREIGN KEY (Code_departemental) REFERENCES Departement(Code_departemental) ON DELETE CASCADE;

INSERT INTO Etablissement SELECT DISTINCT n3, n1, n2, n4, n6, n7, n8 FROM import;

/*Classement*/
CREATE TABLE Classement(clno SERIAL PRIMARY KEY, COD_AFF_FORM INTEGER,
                        Regroupement_1_effectué_par_les_formations_pour_les_classements CHAR(100), 
                        Rang_du_dernier_appelé_du_groupe_1 CHAR(15),
                        Regroupement_2_effectué_par_les_formations_pour_les_classements CHAR(100), 
                        Rang_du_dernier_appelé_du_groupe_2 CHAR(10),
                        Regroupement_3_effectué_par_les_formations_pour_les_classements CHAR(100), 
                        Rang_du_dernier_appelé_du_groupe_3 CHAR(10));

ALTER TABLE Classement ADD CONSTRAINT classement_fk FOREIGN KEY (COD_AFF_FORM) REFERENCES Formation(COD_AFF_FORM) ON DELETE CASCADE;

INSERT INTO Classement SELECT nextval('classement_clno_seq'),n109, n100, n101, n102, n103, n104, n105 FROM import;

/*Former*/

CREATE TABLE Former(Capacité_de_l_établissement_par_formation INTEGER, Code_UAI CHAR(10), COD_AFF_FORM INTEGER);

ALTER TABLE Former ADD CONSTRAINT former_pk PRIMARY KEY (Code_UAI, COD_AFF_FORM);
ALTER TABLE Former ADD CONSTRAINT formation_fk FOREIGN KEY (COD_AFF_FORM) REFERENCES Formation(COD_AFF_FORM) ON DELETE CASCADE;
ALTER TABLE Former ADD CONSTRAINT etablissement_fk FOREIGN KEY (Code_UAI) REFERENCES Etablissement(Code_UAI) ON DELETE CASCADE;

INSERT INTO Former SELECT n16, n2, n109 FROM import;

/*Candidat*/
CREATE TABLE Candidat (cano SERIAL PRIMARY KEY, COD_AFF_FORM INTEGER,
                        Dont_candidates_pour_une_formation INTEGER,
                        Dont_candidats_ayant_postulé_en_internat CHAR(10),
                        Candidats_néo_bacheliers_généraux_en_phase_principale INTEGER,
                        Candidats_néo_bacheliers_technologiques_en_phase_principale INTEGER,
                        Candidats_néo_bacheliers_professionnels_en_phase_principale INTEGER,
                        Candidats_en_phase_principale INTEGER,
                        Candidats_néo_bacheliers_généraux_en_phase_complémentaire INTEGER,
                        Candidats_néo_bacheliers_technologique_en_phase_complémentaire INTEGER,
                        Candidats_néo_bacheliers_professionnels_en_phase_complémentaire INTEGER,
                        Candidats_en_phase_complémentaire INTEGER,
                        Candidats_classés_par_l_établissement_en_phase_principale INTEGER,
                        Candidats_classés_par_l_établissement_en_phase_complémentaire INTEGER,
                        Candidats_classés_par_l_établissement_en_internat CHAR(10),
                        Candidats_classés_par_l_établissement_hors_internat CHAR(10),
                        Candidats_néo_bacheliers_généraux_classés_par_l_établissement INTEGER,
                        Candidats_néo_bacheliers_technologiques_classés_par_l_établissement INTEGER,
                        Candidats_néo_bacheliers_professionnels_classés_par_l_établissement INTEGER,
                        Autres_candidats_classés_par_l_établissement INTEGER,
                        Candidats_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_établissement INTEGER,
                        Autres_candidats_admis INTEGER,
                        Candidats_en_terminale_générale_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_établisement INTEGER,
                        Candidats_en_terminale_technologique_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_etablissement INTEGER,
                        Candidats_en_terminale_professionnelle_ayant_reçu_une_proposition_d’admission_de_la_part_de_l_etablissement INTEGER,
                        Autres_candidats_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_établissemen INTEGER);

ALTER TABLE Candidat ADD CONSTRAINT candidat_fk FOREIGN KEY (COD_AFF_FORM) REFERENCES Formation(COD_AFF_FORM) ON DELETE CASCADE;

INSERT INTO Candidat SELECT nextval('candidat_cano_seq'), n109, n18, n20, n21, n23, n25, n27, n29, n30, n31, n32, n33, n34, n35, n36, 
                            n37, n39, n41, n43, n44, n58, n93, n95, n97, n99 FROM import;

/* Boursier */
CREATE TABLE Boursier(bno SERIAL PRIMARY KEY, cano INTEGER,
                                Candidats_boursiers_néo_bacheliers_généraux_en_phase_principale INTEGER,
                                Candidats_boursiers_néo_bacheliers_technologiques_en_phase_principale INTEGER,
                                Candidats_boursiers_néo_bacheliers_professionnels_en_phase_principale INTEGER,
                                Candidats_boursiers_néo_bacheliers_généraux_classés_par_l_établissement INTEGER,
                                Candidats_boursiers_néo_bacheliers_technologiques_classés_par_l_établissement INTEGER,
                                Candidats_boursiers_néo_bacheliers_professionnels_classés_par_l_établissement INTEGER,
                                Admis_boursiers_néo_bacheliers INTEGER,
                                Candidats_boursiers_en_terminale_générale_ayant_reçu_une_proposition_d_admission_de_l_etablissement FLOAT,
                                Candidats_boursiers_en_terminale_technologique_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_etablissement FLOAT,
                                Candidats_boursiers_en_terminale_générale_professionnelle_ayant_reçu_une_proposition_d_admission_de_la_part_de_l_etablissement FLOAT
                            );

ALTER TABLE Boursier ADD CONSTRAINT boursier_fk FOREIGN KEY (cano) REFERENCES Candidat(cano) ON DELETE CASCADE;

INSERT INTO Boursier SELECT nextval('boursier_bno_seq'), cano, n22, n24, n26, n38, n40, n42, n53, n94, n96, n98 
FROM import INNER JOIN Candidat ON import.n109 = Candidat.cod_aff_form;

/*Admis*/
CREATE TABLE Admi (ano SERIAL PRIMARY KEY, cano INTEGER,
                    Candidates_admises INTEGER,
                    Admis_en_phase_principale INTEGER,
                    Admis_en_phase_complémentaire INTEGER,
                    Admis_ayant_reçu_leur_proposition_d_admission_à_l_ouverture_de_la_procédure_principale INTEGER,
                    Admis_ayant_reçu_leur_proposition_d_admission_avant_le_baccalauréat CHAR(10),
                    Admis_ayant_reçu_leur_proposition_d_admission_avant_la_fin_de_la_procédure_principale INTEGER,
                    Admis_en_internat CHAR(10),
                    Admis_néo_bacheliers INTEGER,
                    Admis_néo_bacheliers_technologiques INTEGER,
                    Admis_néo_bacheliers_professionnels INTEGER,
                    Admis_néo_bacheliers_sans_information_sur_la_mention_au_bac INTEGER,
                    Admis_néo_bacheliers_sans_mention_au_bac INTEGER,
                    Admis_néo_bacheliers_avec_mention_Assez_Bien_au_bac INTEGER,
                    Admis_néo_bacheliers_avec_mention_Bien_au_bac INTEGER,
                    Admis_néo_bacheliers_avec_mention_Très_Bien_au_bac INTEGER,
                    Admis_néo_bacheliers_avec_mention_Très_Bien_avec_félicitations_au_bac INTEGER,
                    Admis_néo_bacheliers_généraux_ayant_eu_une_mention_au_bac INTEGER,
                    Admis_néo_bacheliers_technologiques_ayant_eu_une_mention_au_bac INTEGER,
                    Admis_néo_bacheliers_professionnels_ayant_eu_une_mention_au_bac INTEGER,
                    Admis_issus_du_même_établissement_BTS_CPGE CHAR(10),
                    Admises_issues_du_même_établissement_BTS_CPGE CHAR(10),
                    Admis_issus_de_la_même_académie INTEGER,
                    Admis_issus_de_la_même_académie_Paris_Créteil_Versailles_réunies INTEGER
                    );

ALTER TABLE Admi ADD CONSTRAINT admi_fk FOREIGN KEY (cano) REFERENCES Candidat(cano) ON DELETE CASCADE;

INSERT INTO Admi SELECT nextval('admi_ano_seq'), cano, n46, n47, n48, n49, n50, n51, n52, n54, n56, n57, n59, n60, n61, n62, 
n63, n64, n65,  n66, n67, n68, n69, n70, n71
                        FROM import INNER JOIN Candidat ON import.n109 = Candidat.cod_aff_form;
