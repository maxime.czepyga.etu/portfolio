import random
import time
import matplotlib.pyplot as plt

def jeu():
    '''
Jeu de pierre,feuille,ciseaux renvoyant à chaque manche le numéro de la manche, le glyphe
du joueur 1, le glyphe du joueur 2, le gagnant de la manche et enfin le gagnant de la partie
avec son score
'''
    joueur_1 = input(str('Quel est le non du premier joueur?'))
    joueur_2 = input(str('Quel est le nom du deuxième joueur?'))
    manches_gan = int(input('En combien de manches gagnantes cette partie se joue-t-elle?'))
    t = int(input('Combien de temps (en secondes) séparent 2 manches?'))
    list = []
    score_1=0
    score_2=0
    nbr_manche = 1
    manches_gan = manches_gan + 1
    x = []
    y = []
    a = []
    b = []
    p = 0
    while manches_gan!= nbr_manche:
        glyphe_1= random.choice(["\N{RAISED FIST}","\N{RAISED HAND}","\N{VICTORY HAND}"])
        glyphe_2= random.choice(["\N{RAISED FIST}","\N{RAISED HAND}","\N{VICTORY HAND}"])
        if glyphe_1==glyphe_2:
            list.append([nbr_manche ,glyphe_1 ,glyphe_2 ,0])
        else:
            x.extend([score_1])
            y.extend([p])
            if glyphe_1=="\N{RAISED FIST}":
                if glyphe_2=="\N{RAISED HAND}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,2])
                    score_2 = score_2 + 1
                elif glyphe_2=="\N{VICTORY HAND}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,1])
                    score_1 = score_1 + 1
                    x.extend([score_1])
                    y.extend([p])
            if glyphe_1=="\N{RAISED HAND}":
                if glyphe_2=="\N{RAISED FIST}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,1])
                    score_1 = score_1 + 1
                    x.extend([score_1])
                    y.extend([p])
                elif glyphe_2=="\N{VICTORY HAND}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,2])
                    score_2 = score_2 + 1
            if glyphe_1=="\N{VICTORY HAND}":
                if glyphe_2=="\N{RAISED FIST}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,2])
                    score_2 = score_2 + 1
                elif glyphe_2=="\N{RAISED HAND}":
                    list.append([nbr_manche,glyphe_1,glyphe_2,1])
                    score_1 = score_1 + 1
                    x.extend([score_1])
                    y.extend([p])
            nbr_manche = nbr_manche + 1
            time.sleep(t)
            p = p+1
            print(list)
    if score_1<score_2:
        print('Le gagnant est',joueur_2,', son score est',score_2,'et', joueur_1,'a fais', score_1 )
    elif score_1==score_2:
        print('EGALITE')
    else:
        print('Le gagnant est',joueur_1,', son score est',score_1,'et',joueur_2,'a fais',score_2)

def voir_schéma():
    plt.plot(x,y)
    
    plt.xlabel("nombre de victoire cumulées")
    plt.ylabel("passage numéro")
    plt.title("Evolution du nombre de manches gagnées pendant la partie")
    plt.show()
