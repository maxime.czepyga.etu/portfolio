###########################################################
#                                                         #
#                     Importation module                  #
#                                                         #
###########################################################
from random import random
from random import randint
from copy import copy

###########################################################
#                                                         #
#                         Affichage                       #
#                                                         #
###########################################################

def afficher_joueur(joueur_courant):
    '''Fonction qui affiche le joueur courant.
: param: joueur courant
: paramtype: str(joueur_courant)
: return: None
'''
    print('C\' est à ', joueur_courant, ' de jouer.')

def afficher_coups(coups):
    '''Fonction qui affiche les coups possibles.
: param: coups
: paramtype: lst(coups)
: return: None
'''
    print('Voici les coups possibles : ', coups)

def afficher_choix(choix, joueur_courant):
    '''Fonction qui affiche le choix du joueur courant.
: param: choix,
         joueur courant.
: paramtype: int(choix), str(joueur_courant)
: return: None
'''
    print(joueur_courant, ' a joué : ', choix)

def afficher_situation(situation):
    '''Fonction qui donne un apperçu graphique de la situation.
: param: situation
: paramtype: lst(situation)
: return: None
'''
    for j in range(len(situation)):
        print('+---+---+---+---+---+---+---+')
        for i in range(len(situation[0])):
            print('|', situation[j][i], end=" ")
        print('|')
    print('+---+---+---+---+---+---+---+')
    print('  1   2   3   4   5   6   7  ')
            
def resultat(test, joueur_courant):
    '''Fonction qui affirme si le match et nul sinon renvoie le nom du vainqueur.
: param: test,
         joueur_courant.
: paramtype: bool(test), str(joueur_courant)
: return: affirme que le match et nul ou donne le nom du vainqueur
: returntype: str('Match nul'), str(nom du vainqueur)
'''
    if test == False:
        return ('Match nul')
    return ('vainqueur : ' + joueur_courant)

###########################################################
#                                                         #
#                  Initialisation du jeu                  #
#                                                         #
###########################################################

def initialiser_jeu():
    '''Fonction qui initialise un jeu.
: param: None
: return: une liste 2 dimensions (7 par 6) qui représente une grille puissance 4
: returntype: lst(grille)
'''
    return [[' ' for i in range(7)]for j in range(6)]

###########################################################
#                                                         #
#                  fonction utilitaire                    #
#                                                         #
###########################################################

def recherche_symbole(joueur_courant, joueur1):
    '''Fonction qui recherche le symbole du joueur courant.
: param: joueur_courant,
         joueur1.
: paramtype: str(joueur_courant), str(joueur1)
: return: le symbole du joueur courant
: returntype: str(symbole)
'''
    if joueur_courant == joueur1:
        return 'X'
    return 'O'

###########################################################
#                                                         #
#                  Fonctions liées aux coups              #
#                                                         #
###########################################################
def existence_coup(situation):
    '''Prédicat qui donne l'existance d'un coup ou non.
: param: situation
: paramtype: lst(situation)
: return: une booléen
: returntype: bool(booléen)
'''
    liste_commune = []
    a = 0
    for i in situation:
        liste_commune += situation[a]
        a += 1
    for i in liste_commune:
        if i != 'X' and i != 'O':
            return True
    return False

def coups_autorises(situation):
    '''Fonction qui renvoie les colones dans lesqu'elles le joueur peut placer son jeton.
: param: le jeu actuel
: paramtype: lst(jeu_actuel)
: return: les colone dans lequelles le joueur peut jouer
: returntype: lst(colones)
'''
    endroits_libres = []
    coups_possibles = []
    coups_possibles_bis = []
    nb_elements_1 = len(situation)
    a=0
    while a < nb_elements_1:
        nb_elements_2 = len(situation[a])    
        b = 0
        while b < nb_elements_2:
            if situation[a][b] != 'X' and situation[a][b] != 'O':
                endroits_libres.append(b)
            b = b + 1
        a = a + 1
    for i in endroits_libres:
        if i not in coups_possibles:
            coups_possibles.append(i)
    coups_possibles.sort()
    for i in coups_possibles:
        coups_possibles_bis.append(i+1)
    return coups_possibles_bis

def choix_coup(liste_coups):
    '''Fonction qui renvoie le choix du joueur.
: param: la liste des coups possibles
: paramtype: lst(coups_possibles)
: return: choix du coup du joueur
: returntype: int(choix_coup)
'''
    autorisation = False
    while autorisation != True:
        y = int(input('Sur qu\'elle colone voulez vous jouer?'))
        for i in liste_coups:
            if i == y:
                autorisation = True
    return (y)

###########################################################
#                                                         #
#                  Mise à jour situation                  #
#                                                         #
###########################################################

def miseajour_situation(choix, situation, joueur_courant, joueur1):
    '''Fonction qui met à jour la grille de jeu en fonction de ce que le joueur à choisis en coup.
: param: choix,
         situation,
         joueur_courant,
         joueur1.
: paramtype: int(choix), lst(situation), str(joueur_courant), lst(joueur1)
: return: la grille de jeu mise à jour
: returntype: lst(grille)
'''
    symbole = recherche_symbole(joueur_courant, joueur1)
    situation_bis = copy(situation)
    situation_bis.reverse()
    for i in situation_bis:
        if i[choix - 1] == ' ':
            i[choix - 1] = symbole
            situation_bis.reverse()
            return situation_bis

###########################################################
#                                                         #
#                  Détection fin du jeu                   #
#                                                         #
###########################################################

def test_four_cases(i, j, situation, symbole, di, dj):
    '''Prédicat qui teste si 4 symboles sont alignés en commençant par les coordonnées mis en paramètres
: param: i,
         j,
         situation,
         symbole,
         di,
         dj.
: paramtype: int(i), int(j), lst(situation), str(symbole), int(di), int(dj)
: return: Un bolléen, True ou False, si 4 symboles sont allignés
: returntype: bool(réponse)
'''
    acc = 0
    if di == -1 and dj == 0:
        for _ in range(4):
            if situation[i][j] == symbole:
                acc += 1
            i -= 1
    elif di == 0 and dj == 1:
        for _ in range(4):
            if situation[i][j] == symbole:
                acc += 1
            j += 1
    elif di == -1 and dj == 1: ##diagonales droites
        for _ in range(4):
            if situation[i][j] == symbole:
                acc += 1
            j += 1
            i -= 1
    elif di == -1 and dj == -1: ##diagonales gauches
        for _ in range(4):
            if situation[i][j] == symbole:
                acc += 1
            j -= 1
            i -= 1
    if acc == 4:
        return True
    return False
            
def test_lignes(situation, symbole):
    '''Prédicat qui teste si 4 symboles sont alignés sur une ligne.
: param: situation,
         symbole,
: paramtype:lst(situation), str(symbole)
: return: Un bolléen, True ou False, si 4 symboles sont allignés
: returntype: bool(réponse)
'''
    a = 0
    for i in range(len(situation)):
        for j in range(len(situation[0])-3):
            prédicat = test_four_cases(((len(situation)-1)-i), j, situation, symbole, 0, 1)
            if prédicat == True:
                return prédicat
        a += 1
    return prédicat

def test_colonnes(situation, symbole):
    '''Prédicat qui teste si 4 symboles sont alignés sur une colone.
: param: situation,
         symbole,
: paramtype:lst(situation), str(symbole)
: return: Un bolléen, True ou False, si 4 symboles sont allignés
: returntype: bool(réponse)
'''
    a = 0
    for i in range(len(situation)-4):
        for j in range(len(situation[0])):
            prédicat = test_four_cases(((len(situation)-1)-i), j, situation, symbole, -1, 0)
            if prédicat == True:
                return prédicat
        a += 1
    return prédicat

def test_diagonales(situation, symbole):
    '''Prédicat qui teste si 4 symboles sont alignés sur une diagonale.
: param: situation,
         symbole,
: paramtype:lst(situation), str(symbole)
: return: Un bolléen, True ou False, si 4 symboles sont allignés
: returntype: bool(réponse)
'''
    a = 3
    for i in range(len(situation)-3,len(situation)):
        b = 0
        for j in range(len(situation[0])-3):
            prédicat = test_four_cases(a, b, situation, symbole, -1, 1)
            b += 1
            if prédicat == True:
                    return prédicat
        a += 1

    a = 3
    for i in range(len(situation)-3,len(situation)):
        b = 3
        for j in range(len(situation[0])-4,len(situation[0])):
            prédicat = test_four_cases(a, b, situation, symbole, -1, -1)
            b += 1
            if prédicat == True:
                    return prédicat
        a += 1

def tester(situation, joueur_courant, joueur1):
    '''Fonction qui réalise l'ensemble des tests pour savoir si une partie est gagnée ou perdue.
: param: situation
         joueur_courant,
         joueur1
: paramtype: lst(situation), str(joueur_courant), str(joueur1)
: return: True ou False
: returntype: bool(reponse)
'''
    resultat = []
    symboles = ['X','O'] 
    symbole_1 = recherche_symbole(joueur_courant, joueur1)
    symboles.remove(symbole_1)
    symbole_2 = symboles[0]
    resultat.append(test_lignes(situation, symbole_1))
    resultat.append(test_lignes(situation, symbole_2))
    resultat.append(test_colonnes(situation, symbole_1))
    resultat.append(test_colonnes(situation, symbole_2))
    resultat.append(test_diagonales(situation, symbole_1))
    resultat.append(test_diagonales(situation, symbole_2))
    for i in resultat:
        if i == True:
            return True
    return False
    
def fin_jeu(test, joueur_courant, situation):
    '''Fonction qui déclare la fin de jeu.
: param: test,
         joueur_courant,
         situation.
: paramtype: bool(test), str(joueur_courant), lst(situation)
: return: resultats
: returntype: func(resultat)
'''
    afficher_situation(situation)
    return resultat(test, joueur_courant)

###########################################################
#                                                         #
#                Fonctions principales du jeu             #
#                                                         #
###########################################################

def nom_joueurs():
    '''Fonction qui demande le nom des joueurs pour engager la partie.
: param: None
: return: Un tuple avec le joueur 1 et le joueur 2.
: returntype: tuple(joueurs)
'''
    j1 = str(input('Quel est le nom du joueur 1?'))
    j2 = str(input('Quel est le nom du joueur 2?'))
    return (j1,j2)

def premier_joueur(joueur1, joueur2):
    '''Fonction qui initialise le premier joueur tité au sort.
: param: joueur 1,
         joueur 2.
: paramtype: str(joueur)
: return: le joueur tiré au sort
: returntype: str(joueur)
'''
    return [joueur1, joueur2][randint(0,1)]

def changer_joueur(joueur_courant, joueur1, joueur2):
    '''Fonction qui change le joueur courant.
: param: joueur courant,
         joueur 1,
         joueur 2.
: paramtype: str(joueur_courant), str(joueur_1), str(joueur_2)
: return: le nouveau joueur courant
: returntype: str(joueur_courant)
'''
    if joueur_courant == joueur1:
        return joueur2
    return joueur1
    
def jouer():
    '''Fonction principale qui engage la partie.
: param: None
: return: appel de la fonction resultat qui donne le résultat de la partie
: returntype: str(resultat)
'''
    fini = False
    joueurs = list(nom_joueurs())
    joueur_1 = joueurs[0]
    joueur_2 = joueurs[1]
    joueur_courant = premier_joueur(joueur_1, joueur_2)
    situation = initialiser_jeu()
    while fini != True:
        afficher_situation(situation)
        afficher_joueur(joueur_courant)
        coups = coups_autorises(situation)
        afficher_coups(coups)
        choix = choix_coup(coups)
        afficher_choix(choix, joueur_courant)
        situation = miseajour_situation(choix, situation, joueur_courant, joueur_1)
        joueur_courant = changer_joueur(joueur_courant, joueur_1, joueur_2)
        fini = tester(situation, joueur_courant, joueur_1)
    afficher_situation(situation)
    joueur_courant = changer_joueur(joueur_courant,joueur_1, joueur_2)
    return(resultat(fini, joueur_courant))

