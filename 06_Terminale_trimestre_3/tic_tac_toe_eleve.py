###########################################################
#                                                         #
#                     Importation module                  #
#                                                         #
###########################################################
from random import randint

###########################################################
#                                                         #
#                         Affichage                       #
#                                                         #
###########################################################

#[['X','O','X'],['X','X','O'],['O','X','O']]

def affichage_joueur(joueur_courant):
    """
    permet d'afficher le nom du joueur qui a la main
    :param: joueur_courant represente le joueur qui a la main
    :param type: str(joueur_courant)
    :return: None
    """
    print('c est à ',joueur_courant, 'de jouer')

def affichage_situation(situation):
    """
    permet d'afficher la grille de jeu
    :param: situation represente la grille de jeu
    :param type: list(situation)
    :return: None
    """
    for j in range(3):
        print('+---+---+---+')
        for i in range(3):
            print('|', situation[j][i], end=" ")
        print('|')
    print('+---+---+---+')
    
def affichage_coups(coups):
    """
    permet d'afficher les coups possibles
    :param: coups represente un tuple de l'ensemble des coups possibles
    :param type: tuple(coups)
    :return: None
    """
    print(coups)

def afficher_choix(choix, joueur_courant):
    """
    permet d'afficher le coup choisi
    :param: choix represente les coordonnees du coup choisi
    joueur_courant represente le joueur qui a la main
    :param type: tuple(choix), str(joueur_courant)
    :return: None
    """
    print(joueur_courant, ' à choisie ', choix)

def resultat(test, joueur_courant):
    """
    permet d'afficher le vainqueur du jeu ou match nul si il n'y a pas de vainqueur
    :param: test represente un booleen qui verifie si il y a victoire d'un des deux joueur
    joueur_courant represente le joueur qui a la main
    :param type: bool(test), str(joueur_courant)
    :return: None
    """
    if test == True:
        print(joueur_courant)
    else:
        print('match nul')

###########################################################
#                                                         #
#                  Initialisation du jeu                  #
#                                                         #
###########################################################

def initialiser_jeu():
    """
    permet d'initialiser le jeu tic-tac-toc
    :param: None
    :param type: None
    :return: retourne une grille vide
    :rtype: str
    """
    return [[' ' for i in range(3)] for j in range(3)]

###########################################################
#                                                         #
#                  fonction utilitaire                    #
#                                                         #
###########################################################

def recherche_symbole(joueur_courant, joueur1):
    """
    permet de retourner le symbole du joueur_courant, "X" pour le joueur 1 et "O" pour le joueur 2
    :param: joueur_courant represente le joueur qui a la main
    joueur1 represente le nom du joueur numero 1
    :param type: str(joueur_courant), str(joueur1)
    :return: "X" si le joueur 1 est le joueur courant sinon "O"
    :rtype: str
    """
    if joueur_courant == joueur1:
        return 'X'
    else:
        return 'O'

###########################################################
#                                                         #
#                  Fonctions liées aux coups              #
#                                                         #
###########################################################

def existence_coup(situation):
    """
    predicat qui verifie si un coup est encore possible
    :param: situation represente la grille de jeu durant le tour
    :param type: str(situation)
    :return: renvoie True si il existe au moin un possibilite sinon False
    :rtype: bool
    """
    for i in situation:
        for j in i:
            if j == ' ':
                return True
    return False

def coups_autorises(situation):
    """
    permet de determiner les coups possible 
    :param: situation represente la grille de jeu durant le tour
    :param type: str(situation)
    :return: renvoie un tuple des coups possibles pour le joueur_courant
    :rtype: tuple
    """
    coups_possibles = []
    nb_elements_1 = len(situation)
    a = 0
    while a < nb_elements_1:
        nb_elements_2 = len(situation[a])
        b = 0
        while b < nb_elements_2:
            if situation[a][b] != 'X' and situation[a][b] != 'O':
                coups_possibles.append((a,b))
            b = b + 1
        a = a + 1
    return coups_possibles

def choix_coup(coups_possible):
    """
    permet de choisir un coup parmi les coups autorise
    :param: liste_coups represente un tuple de l'ensemble des coups possibles
    :param type: tuple(liste_coups)
    :return: renvoie un tuple avec les coordonnees du coup choisi
    :rtype: tuple
    """
    print('voici la liste de coups possible :')
    affichage_coups(coups_possible)
    choix1 = int(input('Sur qu\'elle ligne voulez vous jouer? '))
    choix2 = int(input('Sur qu\'elle colone voulez vous jouer? '))
    return (choix1,choix2)

###########################################################
#                                                         #
#                  Mise à jour situation                  #
#                                                         #
###########################################################

def miseajour_situation(choix, situation, joueur_courant, joueur1):
    """
    permet de mettre a jour la situation suite au coup joue
    :param: choix represente les coordonnees du coup choisi
    situation represente la grille de jeu durant le tour
    joueur_courant represente le joueur qui a la main
    joueur1 represente le nom du joueur numero 1
    :param type: tuple(choix), str(situation), str(joueur_courant), str(joueur1)
    :return: retourne la grille de jeu apres le coup joue
    :rtype: tuple
    """
    situation[choix[0]][choix[1]] = recherche_symbole(joueur_courant, joueur1)
    return situation
        
###########################################################
#                                                         #
#                  Détection fin du jeu                   #
#                                                         #
###########################################################

def tester(situation, joueur_courant, joueur1):
    """
    permet de tester si il y a victoire pour un joueur ou non
    :param: situation represente la grille de jeu durant le tour
    joueur_courant represente le joueur qui a la main
    joueur1 represente le nom du joueur numero 1
    :param type: str(situation), str(joueur_courant), str(joueur1)
    :return: renvoie True si il y a victoire sinon False
    :rtype bool
    """
    symbole = recherche_symbole(joueur_courant, joueur1)
    nb_elements_1 = len(situation)
    nb_elements_2 = len(situation[0])
    a = 0
    b = 0
    while a < nb_elements_1:
        if situation[a][0] == symbole or situation[a][1] == symbole or situation[a][2] == symbole:
            if situation[a][0] == situation[a][1] and situation[a][0] == situation[a][2]:
                return True
        a = a + 1
    while b < nb_elements_2:
        if situation[0][b] == symbole or situation[1][b] == symbole or situation[2][b] == symbole:
            if situation[0][b] == situation[1][b] and situation[0][b] == situation[2][b]:
                return True
        b = b + 1
    if situation[0][0] == symbole or situation[1][1] == symbole or situation[2][2] == symbole:
        if situation[0][0] == situation[1][1] and situation[0][0] == situation[2][2]:
            return True
    if situation[0][2] == symbole or situation[1][1] == symbole or situation[2][0] == symbole:
        if situation[0][2] == situation[1][1] and situation[0][2] == situation[2][0]:
            return True
    return False
        
    
def fin_jeu(test, situation):
    """
    predicat permet de mettre permet de determiner si le jeu est termine
    :param: test represente un booleen qui verifie si il y a victoire d'un des deux joueur
    situation represente la grille de jeu durant le tour
    :param type: bool(test), str(situation), str(joueur_courant)
    :return: renvoie True si le jeu est fini sinon False
    :rtype: bool
    """
    if test == True or existence_coup(situation) == False:
        return True
    else:
        return False



###########################################################
#                                                         #
#                Fonctions principales du jeu             #
#                                                         #
###########################################################

def nom_joueurs():
    """
    permet d'ajouter le nom des joueurs
    :param: None
    :return: retourne un tuple avec le nom des deux joueurs
    :rtype: tuple
    """
    joueur1 = input('joueur 1: ')
    joueur2 = input('joueur 2: ')
    return (joueur1, joueur2)

def premier_joueur(joueur1, joueur2):
    """
    permet de choisir aleatoirement le premier joueur qui pourra jouer
    :param: joueur1 represente le nom du joueur 1
    joueur2 represente le nom du joueur 2
    :param type: str(joueur1), str(joueur2)
    :return: retourne le nom du premier joueur
    :rtype: str
    """
    acc = randint(0,1)
    if acc == 0:
        return joueur1
    else:
        return joueur2

def changer_joueur(joueur_courant, joueur1, joueur2):
    """
    permet de changer de joueur_courant
    :param: joueur_courant represente le joueur qui a la main
    joueur1 represente le nom du joueur 1
    joueur2 represente le nom du joueur 2
    :param type: str(joueur_courant), str(joueur1), str(joueur2)
    :return: retourne joueur1 si joueur_courant est le joueur2 et inversement
    :rtype: str
    """
    if joueur_courant == joueur1:
        return joueur2
    else:
        return joueur1

def jouer():
    joueur= nom_joueurs()
    joueur_courant= premier_joueur(joueur[0],joueur[1])
    situation = initialiser_jeu()
    fin_du_jeu = False
    while fin_du_jeu == False:
        affichage_situation(situation)
        affichage_joueur(joueur_courant)
        coups = coups_autorises(situation)
        choix = choix_coup(coups)
        situation = miseajour_situation(choix, situation, joueur_courant, joueur[0])
        test = tester(situation, joueur_courant, joueur[0])
        fin_du_jeu = fin_jeu(test,situation)
        joueur_courant = changer_joueur(joueur_courant,joueur[0],joueur[1])
    affichage_situation(situation)
    joueur_courant = changer_joueur(joueur_courant,joueur[0],joueur[1])
    resultat(test, joueur_courant)
    
