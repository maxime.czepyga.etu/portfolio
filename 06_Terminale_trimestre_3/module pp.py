import time
from random import random
import sys

########################################################################
#                                                                      #
#                     Module de choix du jeu                           #
#                                                                      #
########################################################################

def jeu():
    '''Fonction qui exécute l'un des jeux selon le choix de l'utilisateur. Les jeux
sont importés sous forme de modules.
: param: None
: return: None
'''
    choix = str(input('A quel jeu voulez-vous jouer? (puissance4 ou tic_tac_toe)'))
    while True:
        if choix == 'tic_tac_toe':
            sys.path.append('jeux')
            return __import__('tic_tac_toe_eleve').jouer()
        elif choix == 'puissance4':
            sys.path.append('jeux')
            return __import__('puissance4_eleve').jouer()
        else:
            choix = str(input('A quel jeu voulez-vous jouer? (puissance4 ou tic_tac_toe)'))
