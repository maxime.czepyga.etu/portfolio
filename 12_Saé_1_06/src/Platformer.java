import extensions.*;
class Platformer extends Program {
	boolean victory = false, dead = false;

    // oldChar est la commande précédente. Elle sert à calculer le mouvement à effectuer dans le cas où newChar = SPACE.
    // si oldChar = RIGHT et newChar = SPACE, alors le joueur saute vers la droite.
    // pour sauter sur place, on met oldChar à UP et newChar à SPACE.
    char oldChar, newChar;

	char[][] level;

    final char SPACE = ' ', RIGHT = 'r', LEFT = 'l', DOWN = 'd', UP = 'u', ERROR = 'e', X = 'X', STAR = '☆';

	// représente la durée pour laquelle le joueur va gagner de l'altitude après avoir sauté / va être invincible après avoir récupéré une étoile
	final int JUMP_TIME = 1000;
	final int STAR_TIME = 3000;

	Player p1 = newPlayer("Milan", 0, 0);

	// ensemble des caractères représentant une échelle
	final char[] LADDER = new char[]{'┠','┨'};
	// ensemble des caractères considérés comme étant un espace vide
	final char[] EMPTY = new char[]{'A', 'B', 'C', SPACE, X, STAR};

    // cherche dans le dossier levels et renvoie (sous forme d'un tableau à 2 dim de char) le niveau correspondant à partir d'un entier donné en paramètre
    char[][] newLevel (int lvlN) {
		File lvl = newFile("level_" + lvlN + ".txt");
		String[] temp = new String[34];
		// 34 car nombre de lignes d'un niveau
		// le niveau est stocké ligne par ligne dans un tableau de String
		for (int cpt = 0; cpt < 34; cpt++) {
			temp[cpt] = readLine(lvl);
		}
		char[][] res = new char[34][length(temp[0])];
		for (int i = 0; i < 34; i++) {
			for (int j = 0; j < length(temp[i]); j++) {
				res[i][j] = charAt(temp[i], j);
			}
		}
		return res;
    }

	void updateLevel(char[][] lvl, int lvlNumber, int[][] levelsInfo){
		lvl[levelsInfo[lvlNumber -1][3]][levelsInfo[lvlNumber -1][4]] = 'A';
		lvl[levelsInfo[lvlNumber -1][5]][levelsInfo[lvlNumber -1][6]] = 'B';
		lvl[levelsInfo[lvlNumber -1][7]][levelsInfo[lvlNumber -1][8]] = 'C';
	}

    void testLevel() {
		char[][] level_1 = newLevel(1);
		assertEquals(34, length(level_1, 1));
    }

	// chaque niveau étant d'une difficulté différente, il faut différents temps pour les compléter. Chaque niveau a un timer associé qui est enregistré dans le fichier levels.csv
	// de même, chaque question a une bonne réponse qui peut varier. La bonne réponse est stockée dans le fichier questions.csv
	// On remplit une variable globale pour y avoir accès dans le void algorithm
	void fillCSV(int[][] levels, char[][] questions){
		CSVFile levelTable = loadCSV("levels.csv");
		CSVFile questionTable = loadCSV("question.csv");
		// on remplit le 1er
		for (int i = 0; i < length(levels, 1); i++){
			for (int j = 0; j < length(levels, 2); j++){
				levels[i][j] = stringToInt(getCell(levelTable, i, j));
			}
		}
		// on remplit le 2e
		for (int c1 = 0; c1 < length(questions, 1); c1++){
			for (int c2 = 0; c2 < length(questions, 2); c2++){
				questions[c1][c2] = charAt(getCell(questionTable, c1, c2), 1);
			}
		}
	}


    String QndA (int startOfLine){
		String res = "\n"; int lineNumber = 0;
    	File qFile = newFile("Q_A.txt");
		while (ready(qFile) || lineNumber < startOfLine + 2){
			if (lineNumber == startOfLine || lineNumber == startOfLine + 1){
				res += readLine(qFile) + "\n";
			} else {
				readLine(qFile);
			}
			lineNumber++;
		}
		return res;
    }

    void testQndA() {
	// vérifie que la fonction affiche bien les questions demandées
		int i = 1; 
		while (i != 7){
			displayQndA(i);
			i++;
		}
    }

    void displayQndA(int questionNum){
		println(QndA((questionNum-1)*2));
    }

    // renvoie un entier au hasard compris entre min et max inclus
    int randomBtw(int min, int max){
		return ((int) (random()*(max - min))) + min;
    }

    void testRandomBtw() {
		int rand;
		for (int i = 0; i < 200; i++){
	    	rand = randomBtw(12, 50);
	    	assertTrue(rand <= 50 && 12 >= 12);
		}
	}

	// retourne true si c est dans tab, false sinon
	boolean isInTable(char c, char[] tab){
		for (int i = 0; i < length(tab); i++){
			if (c == tab[i]){
				return true;
			}
		}
		return false;
	}

	void testIsInTable(){
		char[] testTab = new char[]{'s', 'A', '5', '║'};
		assertTrue(isInTable('s',testTab));
		assertTrue(isInTable('5',testTab));
		assertTrue(isInTable('║',testTab));
		assertFalse(isInTable('9',testTab));
		assertFalse(isInTable('═',testTab));
	}

    // affiche le niveau. Si les compteurs i et j correspondent à la position du joueur, alors affiche le caractère représentant le joueur
    void display (char[][] tab, Player player, int time, String question) {
		for (int i = 0; i < length(tab, 1); i++) {
	    	for (int j = 0; j < length(tab, 2); j++) {
				if (i == 1 && j == 85) {
		    		if (MStoS(time) >= 10) {
						print(MStoS(time));
		    		} else {
						print("0"+MStoS(time));
		   			}
				} else if (i == player.x && j == player.y) {
		    		print('*');
				} else {
		    		print(tab[i][j]);
				}
	    	}
	    	println();
		}
		println(question);
    }

    // crée un joueur à partir d'un nom et de positions donnés en paramètres
    Player newPlayer(String name, int x, int y){
		Player p = new Player();
		p.name = name;
		p.x = x;
		p.y = y;
		return p;
    }

    void testNewPlayer() {
		Player p = newPlayer("milan", 7, 10);
		assertEquals("milan", p.name);
		assertEquals(7, p.x);
		assertEquals(10, p.y);
    }

    // renvoie une chaîne de caractères contenant le nom et la position d'un joueur 
    String toString(Player p) {
		return (p.name + " " + p.x + " " + p.y);
    }

    void testToStringNP() {
	Player p = newPlayer("milan", 12, 8);
		assertEquals("milan 12 8", toString(p));
    }

	// convertit un temps en milisecondes en un temps en secondes
	int MStoS(int ms){
		return (int) ms/1000;
	}

    // efface l'écran et place le curseur en (0,0)
    void trueClearScreen() {
		clearScreen();
		cursor(0,0);
	}

    // vérifie si la commande command est valide en fonction des paramètres p et level 
    boolean isValid(char[][] lvl, Player p){
		if (newChar == ERROR) {
			return false;
		} else {
			switch (newChar){
			case UP:
				return (p.x > 2);
			case DOWN:
				return (isInTable(lvl[p.x +1][p.y], LADDER)
				        || (isInTable(lvl[p.x][p.y], LADDER) && isInTable(lvl[p.x +1][p.y], EMPTY)));
			case RIGHT:
				return ((p.y < 157 && isInTable(lvl[p.x][p.y +1], EMPTY))
				        || isInTable(lvl[p.x][p.y +1], LADDER));
			case LEFT:
				return ((p.y > 1 && isInTable(lvl[p.x][p.y -1], EMPTY))
						|| isInTable(lvl[p.x][p.y -1], LADDER));
			case SPACE:
				return (!isAirborne(lvl, p));
			default:
				return false;
			}
		}
    }

    /*void testIsValid() {
		Player test = newPlayer("p", 15, 15);
		char[][] level = level(1);
		newChar = UP;
		assertFalse(isValid(level, test));
		newChar = LEFT;
		assertFalse(isValid(level, test));
		newChar = RIGHT;
		assertTrue(isValid(level, test));
		newChar = SPACE;
		assertTrue(isValid(level, test));
		newChar = ERROR;
		assertFalse(isValid(level, test));
		}*/

    void keyTypedInConsole(char key) {
        switch (key) {
            case ANSI_UP:
				oldChar = newChar;
                newChar = UP;
                break;
            case ANSI_DOWN:
				oldChar = newChar;
                newChar = DOWN;
                break;
            case ANSI_LEFT:
				oldChar = newChar;
				newChar = LEFT;
				break;
            case ANSI_RIGHT:
				oldChar = newChar;
				newChar = RIGHT;
				break;
	    	case SPACE:
				oldChar = newChar;
				newChar = SPACE;
				break;
	    	default:
	        	newChar = ERROR;
        }
		if(isValid(level, p1)){
			movePlayer(level, p1);
		}
    }

	// fait tomber le joueur si isAirborne vaut true
	void inTheAir(char[][] lvl, Player p){
		if (isAirborne(lvl, p)){
			if (p.jumpTimer == 0){
				p.x++;
			} else {
				p.x--;
				p.jumpTimer -= 250;
			}
		}
	}

	// regarde si le joueur est sur une case vide et si la case en dessous de lui est aussi une case vide et renvoie true si c'est le cas
	boolean isAirborne(char[][] lvl, Player p){
		return isInTable(lvl[p.x][p.y], EMPTY) && isInTable(lvl[p.x +1][p.y], EMPTY);
	}

    // effectue le déplacement du joueur en modifiant les champs x et y du joueur p
    void movePlayer(char[][] lvl, Player p) {
		switch (newChar) {
			case UP:
				if(isInTable(lvl[p.x][p.y], LADDER)){
					p.x--;
				}
				break;
			case DOWN:
				p.x++;
				break;
			case RIGHT:
				p.y++;
				break;
			case LEFT:
				p.y--;
				break;
			case SPACE:
				if (!isAirborne(lvl, p)){
					p.jumpTimer = JUMP_TIME;
					p.x--;
				}
		}

	}

	// on vérifie si le joueur a gagné en vérifiant si sa position correspond à la position de la lettre correspondant à la bonne réponse
	boolean hasWon(char[][] levelTest, int questionNumber, char[][] answers){
		return (levelTest[p1.x][p1.y] == answers[questionNumber -1][1]);
	}

	void testHasWon(){

		int[][] levelStats = new int[3][9];
		char[][] questionAnswers = new char[6][2];

		fillCSV(levelStats, questionAnswers);

		char[][] lvl = newLevel(1);
		updateLevel(lvl, 1, levelStats);

		p1.x = 21;
		p1.y = 1;

		assertTrue(hasWon(lvl, 4, questionAnswers));
		assertFalse(hasWon(lvl, 1, questionAnswers));
	}
    
    void platformerGame() {
		int[][] levelStats = new int[3][9];  	char[][] questionAnswers = new char[6][2];
		fillCSV(levelStats, questionAnswers);

		int level_Number = randomBtw(1, 3);
		level = newLevel(level_Number);
		updateLevel(level, level_Number, levelStats);

		p1.x = levelStats[level_Number -1][1]; p1.y = levelStats[level_Number -1][2];

		trueClearScreen();

		playSound("GreenHillZone.mp3", true);

		int questionNb = randomBtw(1, 6);
		int time = levelStats[level_Number-1][0];
		String question = QndA(questionNb);

		while (time != 0 && !victory && !dead) {
			display(level, p1, time, question);

			enableKeyTypedInConsole(true);

			delay(250);
			inTheAir(level, p1);

			if (hasWon(level, questionNb, questionAnswers)){
				victory = true;
			}
			enableKeyTypedInConsole(false);
			if (p1.starTimer != 0){
				p1.starTimer -= 250;
			}
			if (level[p1.x][p1.y] == STAR){
				p1.starTimer = STAR_TIME;

			}
			if (level[p1.x][p1.y] == X && p1.starTimer == 0){
				dead = true;
			}

			println();
			trueClearScreen();

			time -= 250;
		}
		if (victory){
			playSound("VictoryFanfare.mp3", false);
		}
    }
}
