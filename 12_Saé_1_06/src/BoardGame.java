import extensions.File;

class BoardGame extends Program {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////
    //////                                     INITIALISATION
    //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int SCOREDICE = 0;
    int SCOREFIRSTPLAYER = 1;
    int SCORESECONDPLAYER = 1;

    //c
    int[][] QUESTIONS = new int[][]{{0,1},{0,3},{0,9},{0,13},{1,14},{3,0},{3,6},{4,12},{5,0},{7,2},{7,8},{7,14}};
    //r
    int[][] LABYRINTHS = new int[][]{{0,4},{0,10},{6,1},{2,0},{2,14},{3,5},{3,2},{4,11},{7,13},{7,7},{7,1}};
    //b
    int[][] PLATEFORMER = new int[][]{{05},{0,11},{1,0},{3,14},{3,4},{4,10},{5,14},{5,4},{7,12},{7,6}};
    //                                         0   1   2   3   4   5   6   7   8   9   10  11  12  13  14
    char[][] INITIALBOARDGAME = new char[][]{{'y','c','a','c','r','b','y','a','a','c','r','b','y','c','a'},//0
					     {'b','v','v','v','v','v','r','v','v','v','v','v','v','v','c'},//1
					     {'r','v','v','v','v','v','a','v','v','v','v','v','v','v','r'},//2
					     {'c','a','r','y','b','r','c','v','v','v','v','v','v','v','b'},//3
					     {'a','v','v','v','y','v','v','v','v','v','b','r','c','a','y'},//4
					     {'c','v','v','v','b','v','v','v','v','v','y','v','v','v','b'},//5
					     {'y','v','v','v','a','v','v','v','v','v','a','v','v','v','a'},//6
					     {'b','r','c','a','a','y','b','r','c','a','y','y','b','r','c'}};//7

    ///////////////////////////////////////////////////////////////////////////////
    
    String[] Players(){
	String[] players = new String[2];
	String player;
	println("Name of player one :");
	player = readString();
	players[0] = player;
	println("Name of player two :");
	player = readString();
	players[1] = player;
	return players;
    }

    ///////////////////////////////////////////////////////////////////////////////

    char[] getCharPlayers(String[] players){
	char P1 = 'A';
	char P2 = 'B';
	if (charAt(players[0],0)<123 && charAt(players[0],0)>96){
	    P1 = (char) (charAt(players[0],0)-32);
	} else if (charAt(players[1],0)<123 && charAt(players[1],0)>96){
	    P2 = (char) (charAt(players[1],0)-32);
	} else {
	    P1 = (char) (charAt(players[0],0));
	    P2 = (char) (charAt(players[1],0));
	}
	return new char[]{P1,P2};
    }

    ///////////////////////////////////////////////////////////////////////////////

    char[][] InitialisationBoardGame(char[] players){
	return new char[][]{{players[0] ,players[1] ,'a','c','r','b','y','a','a','c','r','b','y','c','a'},
			    {'b','v','v','v','v','v','r','v','v','v','v','v','v','v','c'},
			    {'r','v','v','v','v','v','a','v','v','v','v','v','v','v','r'},
			    {'c','a','r','y','b','r','c','v','v','v','v','v','v','v','b'},
			    {'a','v','v','v','y','v','v','v','v','v','b','r','c','a','y'},
			    {'c','v','v','v','b','v','v','v','v','v','y','v','v','v','b'},
			    {'y','v','v','v','a','v','v','v','v','v','a','v','v','v','a'},
			    {'b','r','c','a','a','y','b','r','c','a','y','y','b','r','c'}};
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////
    //////                                     DISPLAY
    //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void toString(char[][] BoardGame){
	String cyan = "\u001B[36m";
	String red = "\u001B[31m";
	String blue = "\u001B[34m";
	String yellow = "\u001B[33m";
	String purple = "\u001B[35m";
	String reset = "\u001B[0m";
	print("  _______________________________________________\n");
	for(int firstdim=0; firstdim<length(BoardGame); firstdim++){
	    print(" | "); 
	    for(int seconddim=0; seconddim<length(BoardGame[0]); seconddim++){
		if(BoardGame[firstdim][seconddim]=='c'){
		    print("\u001B[36m"+" O "+"\u001B[0m");
		} else if(BoardGame[firstdim][seconddim]=='r'){
		    print("\u001B[31m"+" O "+"\u001B[0m");
		} else if(BoardGame[firstdim][seconddim]=='b'){
		    print("\u001B[34m"+" O "+"\u001B[0m");
		} else if(BoardGame[firstdim][seconddim]=='y'){
		    print("\u001B[33m"+" O "+"\u001B[0m");
		} else if(BoardGame[firstdim][seconddim]<91){
		    print("\u001B[35m"+" "+BoardGame[firstdim][seconddim]+" "+"\u001B[0m");
	       } else if(BoardGame[firstdim][seconddim]=='a'){
		    print(" O ");
	       } else {
		    print("   ");
	       }
	    }
	    print(" | \n |                                               |\n");
	}
	print(" -------------------------------------------------\n");
	print("Score player one :"+SCOREFIRSTPLAYER+" | Score player two :"+SCORESECONDPLAYER+"\n");
	print("\u001B[36m"+" O "+"\u001B[0m"+" :questions"+" "+"\u001B[31m"+" O "+"\u001B[0m"+" "+" :labyrinths"+" | "+"\u001B[34m"+" O "+"\u001B[0m"+" :plateformer \n");
    }

    ///////////////////////////////////////////////////////////////////////////////

    void readText(String fichier) {
	final String FILENAME = fichier;
	File f = newFile(FILENAME);
	int nbLines = 0, nbChars = 0;
	while (ready(f)) {
	    String currentLine = readLine(f);
	    nbLines++;
	    nbChars = nbChars + length(currentLine);
	    println(currentLine);
	}
    }

    ///////////////////////////////////////////////////////////////////////////////

    void rollDice(char[][] BoardGame){
	String entree = "";
	double proba;
	int total = 0;
	println("Press ENTER to roll the dice!");
	entree = readString();
	for (int cpt=0; cpt<2; cpt++){
	    proba = random();
	    if (proba<=0.17){
		readText("dé1");
		total+=1;
	    } else if (proba>0.17 && proba<=0.34){
		readText("dé2");
		total+=2;
	    } else if (proba>0.34 && proba<=0.51){
		readText("dé3");
		total+=3;
	    } else if (proba>0.51 && proba<=0.68){
		readText("dé4");
		total+=4;
	    } else if (proba>0.68 && proba<=0.85){
		readText("dé5");
		total+=5;
	    } else {
		readText("dé6");
		total+=6;
	    }
	}
	SCOREDICE = total;
	println("Your score :"+total);
	toString(BoardGame);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////
    //////                                     DISPLACEMENT
    //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    char playerChange(char currentPlayer, char firstPlayer, char secondPlayer){
	if (currentPlayer==firstPlayer) return secondPlayer;
	return firstPlayer;
    }

    ///////////////////////////////////////////////////////////////////////////////

    int[] positionPlayer(char[][] BoardGame, char player){
	for (int x=0; x<length(BoardGame); x++){
	    for (int y=0; y<length(BoardGame[0]); y++){
		if (BoardGame[x][y] == player){
		    return new int[]{x,y};
		}
	    }
	}
	return new int[2];
    }

    ///////////////////////////////////////////////////////////////////////////////

    boolean validDisplacement(char[][] BoardGame, int lig, int col){
	if (lig < 0 || lig > 7 || col < 0 || col > 14) return false;
	return (BoardGame[lig][col]=='a' || BoardGame[lig][col]=='c' || BoardGame[lig][col]=='y' || BoardGame[lig][col]=='b' || BoardGame[lig][col]=='r');
    }

    ///////////////////////////////////////////////////////////////////////////////

    char[][] displacement(char[][] BoardGame, char player, int score){
	int[] positionPlayer = positionPlayer(BoardGame, player);
	int x;
	int y;
	String answer = "A";
	while (score>0){
	    do {
		x = positionPlayer(BoardGame,player)[0];
		y = positionPlayer(BoardGame,player)[1];
		println("Top : Z, Bottom : S, Right : D, Left : Q");
		answer += readString();
		if (length(answer)>1 && (charAt(answer,1)=='z' || charAt(answer,1)=='Z')){
		    x--;
		} else if (length(answer)>1 && (charAt(answer,1)=='s' || charAt(answer,1)=='S')){
		    x++;
		} else if (length(answer)>1 && (charAt(answer,1)=='d' || charAt(answer,1)=='D')){
		    y++;
		} else if (length(answer)>1 && (charAt(answer,1)=='q' || charAt(answer,1)=='Q')){
		    y--;
		}
	        answer = "A";
	    } while(!(validDisplacement(BoardGame,x,y)));
	    println("\033[H\033[2J");
	    BoardGame[x][y] = player;
	    BoardGame[positionPlayer[0]][positionPlayer[1]] = INITIALBOARDGAME[positionPlayer[0]][positionPlayer[1]];
	    positionPlayer = positionPlayer(BoardGame, player);
	    score--;
	    toString(BoardGame);
	}
	return BoardGame;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////
    //////                                     REDIRECTIONS
    //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void redirection(int[] positionPlayer, char currentPlayer, char[] players){
	for (int counter=0; counter<length(QUESTIONS); counter++){
	    if (positionPlayer[0]==QUESTIONS[counter][0] && positionPlayer[1]==QUESTIONS[counter][1]){
	        Questions questions = new Questions();
		questions.questionsGame();
		if (currentPlayer==players[0]){
		    SCOREFIRSTPLAYER += questions.WIN;
		} else {
		    SCORESECONDPLAYER += questions.WIN;
		}
	    }
	}
	for (int counter=0; counter<length(LABYRINTHS); counter++){
	    if (positionPlayer[0]==LABYRINTHS[counter][0] && positionPlayer[1]==LABYRINTHS[counter][1]){
		Labyrinths laby = new Labyrinths();
		laby.labyrinthsGame();
		if (laby.WIN==true && currentPlayer==players[0]){
		    SCOREFIRSTPLAYER += 10;
		} else if (laby.WIN==true && currentPlayer==players[1]) {
		    SCORESECONDPLAYER += 10;
		}
	    }
	}
	for (int counter=0; counter<length(PLATEFORMER); counter++){
	    if (positionPlayer[0]==PLATEFORMER[counter][0] && positionPlayer[1]==PLATEFORMER[counter][1]){
	        Platformer platformer = new Platformer();
		platformer.platformerGame();
		if (platformer.victory==true && currentPlayer==players[0]){
		    SCOREFIRSTPLAYER += 10;
		} else if (platformer.victory==true && currentPlayer==players[1]) {
		    SCORESECONDPLAYER += 10;
		}
	    }
	}
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////
    //////                                     ALGORITHM
    //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    
    void boardGame(){
	String[] sPlayers = Players();
	char[] cPlayers = getCharPlayers(sPlayers);
	char[][] BoardGame = InitialisationBoardGame(cPlayers);
	char cCurrentPlayer = cPlayers[0];
	int[] positionPlayer = positionPlayer(BoardGame, cCurrentPlayer);
	while((SCOREFIRSTPLAYER>0 && SCORESECONDPLAYER>0) || (SCOREFIRSTPLAYER<100 && SCORESECONDPLAYER<100)){
	    toString(BoardGame);
	    println("\033[H\033[2J");
	    println(""+cCurrentPlayer +" , it's your turn!");
	    rollDice(BoardGame);
	    BoardGame = displacement(BoardGame, cCurrentPlayer, SCOREDICE);
	    toString(BoardGame);
	    positionPlayer = positionPlayer(BoardGame, cCurrentPlayer);
	    redirection(positionPlayer, cCurrentPlayer, cPlayers);
	    cCurrentPlayer = playerChange(cCurrentPlayer, cPlayers[0], cPlayers[1]);
	}
    }
}
