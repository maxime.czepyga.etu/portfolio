import extensions.CSVFile;

class Questions extends Program {

    int WIN = 0;

    void readCSV(int[] table){
	//Chargement  en mémoire d'un fichier CSV (ici situé dans le même répertoire) dans une variable de type CSVFile
	CSVFile file = loadCSV("questions.csv");
	//Boucle d'affichage
	String cell;
	String question;
	String answer_a;
	String answer_b;
	String answer_c;
	String answer;
	String good_answer;
	for (int lig=0;lig<length(table);lig++){		
	    cell = getCell(file,table[lig],0);
	    question = question(cell);
	    answer_a = answer_a(cell);
	    answer_b = answer_b(cell);
	    answer_c = answer_c(cell);
	    good_answer = goodAnswer(cell);
	    println(question);
	    println("\n"+answer_a+"\n"+answer_b+"\n"+answer_c);
	    answer = readString();
	    if (equals(answer,good_answer)){
		WIN+=1;
	    }
	}
    }

    String question(String cell){
	for (int counter=0; counter<length(cell); counter++){
	    if (charAt(cell,counter)==';'){
		return substring(cell,0,counter);
	    }
	}
	return "";
    }

    String answer_a(String cell){
	int i = 1;
	int start = 0;
	for (int counter=0; counter<length(cell); counter++){
	    if (charAt(cell,counter)==';' && i==0){
		return substring(cell,start+1,counter);
	    } else if (charAt(cell,counter)==';' && i>0){
		i--;
		start=counter;
	    }
	}
	return "";
    }

    String answer_b(String cell){
	int i = 2;
	int start = 0;
	for (int counter=0; counter<length(cell); counter++){
	    if (charAt(cell,counter)==';' && i==0){
		return substring(cell,start+1,counter);
	    } else if (charAt(cell,counter)==';' && i>0){
		i--;
		start=counter;
	    }
	}
	return "";
    }

    String answer_c(String cell){
	int i = 3;
	int start = 0;
	for (int counter=0; counter<length(cell); counter++){
	    if (charAt(cell,counter)==';' && i==0){
		return substring(cell,start+1,counter);
	    } else if (charAt(cell,counter)==';' && i>0){
		i--;
		start=counter;
	    }
	}
	return "";
    }

    String goodAnswer(String cell){
	int i = 4;
	int start = 0;
	for (int counter=0; counter<length(cell); counter++){
	    if (charAt(cell,counter)==';' && i==0){
		return substring(cell,start+1,counter);
	    } else if (charAt(cell,counter)==';' && i>0){
		i--;
		start=counter;
	    }
	}
	return "";
    }

    void questionsGame(){
	int[] table = new int[10];
	for (int counter=0; counter<length(table); counter++){
	    table[counter] = (int) (random()*30);
	}
	readCSV(table);
    }
}
