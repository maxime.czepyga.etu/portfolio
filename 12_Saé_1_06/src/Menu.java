import extensions.*;

class Menu extends Program{
    
    void lecturetexte(String fichier) {
	String result;
	final String FILENAME = fichier;
	File f = newFile(FILENAME);
	int nbLines = 0;
	String currentLine = "";
	while (ready(f)) {
	    currentLine = readLine(f);
	    nbLines++;
	    for (int cpt=0; cpt<length(currentLine); cpt++){
		if (nbLines>=6 && nbLines<=17 && cpt>2 && cpt<128){
		    //   couleur de fond  couleur de texte
		    print("\u001B[32m" + "\u001B[1;34m" +  currentLine.substring(cpt, cpt+1));
		} else {
		    print("\u001B[0m" + "\u001B[1;34m" + currentLine.substring(cpt, cpt+1));
		}
	    }
	    print("\n");
	}
	println("Press ENTER");
	result = readString();
    }

    void lectureson(String fichier){
	//play(fichier, true);
    }

    void menuchoix(){
	println("\n\n1: PLAY                     ");
	println("2: INSTRUCTIONS             ");
	println("3: ANSWERS OF THE QUESTIONS ");
	print("\n");
    }

    int  orientation(){
	int number;
	String result="";
	do {
	    number = readInt();
	    if (number==1){
	        BoardGame boardgame = new BoardGame();
		boardgame.boardGame();
	    } else if (number==2){
		lecturetexte("notice");
	    } else if (number==3){
		readCSV("questions.csv");
	    }
	} while (number<1 || number>3);
	return number;
    }

    void readCSV(String file){
	String result;
	CSVFile File = loadCSV(file);
	int nbLignes = rowCount(File);
	int nbCol = columnCount(File);
	for (int lig=0;lig<nbLignes;lig++){
	    for (int col=0;col<nbCol;col++){		
		String cell = getCell(File,lig,col);
		print(cell + " ");
	    }
	    println();
	}
	println("Press ENTER to quit");
	result = readString();
    }

    void algorithm(){
	//	System.out.println("Working Directory = " + System.getProperty("user.dir"));
	int number;
	playSound("menu.mp3", true);
	do {
	    println("\033[H\033[2J");
	    lecturetexte("title");
	    menuchoix();
	    number = orientation();
	} while (number!=1);
    }	
}
